//
//  ConsumerDashboardModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/04/23.
//

import Foundation
import Metal

class ConsumerDashboardResponse{
    var status : Bool?
    var Data : geDataModel?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = geDataModel(from: dataarray)
        }
    }
}

class geDataModel{
    var rewardPoints : Int?
    var inezContactsCount : Int?
    var recomentedProviderContactsCount : Int?
    var recentRecommendProviders : [recentRecommendProvidersList] = []
    var recentlyJoinedUsers : [recentlyJoinedUsersList] = []
    var recentFeedback : [recentFeedbackList] = []
    var userProfileDetails : consumeruserProfileDetailslist?
    init(from data : [String:Any]){
        self.rewardPoints = data["rewardPoints"] as? Int
        self.inezContactsCount = data["inezContactsCount"] as? Int
        self.recomentedProviderContactsCount = data["recomentedProviderContactsCount"] as? Int
        if let recentRecommend = data["recentRecommendProviders"] as? [[String:Any]]{
            for recprovider in recentRecommend{
                self.recentRecommendProviders.append(recentRecommendProvidersList(from: recprovider))
            }
        }
        if let recentlyJoinedUsers = data["recentlyJoinedUsers"] as? [[String:Any]]{
            for recuser in recentlyJoinedUsers{
                self.recentlyJoinedUsers.append(recentlyJoinedUsersList(from: recuser))
            }
        }
        if let recentFeedbacks = data["recentFeedback"] as? [[String:Any]]{
            for recfeedback in recentFeedbacks{
                self.recentFeedback.append(recentFeedbackList(from: recfeedback))
            }
        }
        if let dataarray = data["userDetails"] as? [String:Any]{
            self.userProfileDetails = consumeruserProfileDetailslist(from : dataarray)
        }
    }
}

class recentRecommendProvidersList{
    var id:  String?
    var userId: String?
    var name : String?
    var countryCode: String?
    var phoneNumber: String?
    var contactImage: String?
    var isUser : Bool?
    var contactUserId : String?
    var type : String?
    var isRecommended : Bool?
    var categoryIds: [categoryIdslist] = []
    var areaOfService : [recomareaOfServicelist] = []
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.userId = data["userId"] as? String
        self.name = data["name"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.contactImage = data["contactImage"] as? String
        self.contactUserId = data["contactUserId"] as? String
        self.type = data["type"] as? String
        self.isUser = data["isUser"] as? Bool ?? false
        self.isRecommended = data["isRecommended"] as? Bool ?? false
        if let recentContacts = data["categoryIds"] as? [[String:Any]]{
        for recomcontacts in recentContacts{
            self.categoryIds.append(categoryIdslist(fromData: recomcontacts))
          }
        }
        if let recentContacts = data["areaOfService"] as? [[String:Any]]{
            for recomcontacts in recentContacts{
                self.areaOfService.append(recomareaOfServicelist(fromData: recomcontacts))
            }
        }
   }

    
    
//    var contacts : Recommendcontactslist?
//    init(from data : [String:Any]){
//        if let recentContacts = data["contacts"] as? [String:Any]{
//            self.contacts = Recommendcontactslist(from : recentContacts)
//        }
//   }
}

//class Recommendcontactslist{
//
//}

class categoryIdslist{
    var _id: String?
    var name : String?
    var categoryImage : String?
    init(fromData data: [String:Any]) {
        self._id = data["_id"] as? String
        self.name = data["name"] as? String
        self.categoryImage = data["categoryImage"] as? String
    }
}

class recomareaOfServicelist{
    var city : String?
    var stateShortCode : String?
    init(fromData data: [String:Any]) {
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
    }
}

class recentlyJoinedUsersList{
    var contactUserId : recentlycontactUserIdList?
    init(from data : [String:Any]) {
        if let dataarray = data["contactUserId"] as? [String:Any]{
            self.contactUserId = recentlycontactUserIdList(from: dataarray)
        }
    }
    
}

class recentlycontactUserIdList{
    var id: String?
    var fullName: String?
    var profilePic: String?
    var city: String?
    var state: String?
    init(from data:[String:Any]){
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.profilePic = data["profilePic"] as? String
        self.city = data["city"] as? String
        self.state = data["state"] as? String
    }
}

class recentFeedbackList{
    var id:  String?
    var feedback:   String?
    var categoryIds: categoryIdsList?
    var type:   String?
    var createdAt:  String?
    var userId : recuserIdlist?
    var providerId : providerIdlist?
    init(from data:[String:Any]){
        self.id = data["_id"] as? String
        self.feedback = data["feedback"] as? String
        self.type = data["type"] as? String
        self.createdAt = data["createdAt"] as? String
        if let dataarray = data["categoryIds"] as? [String:Any]{
            self.categoryIds = categoryIdsList(fromData: dataarray)
        }
        if let dataarray = data["userId"] as? [String:Any]{
            self.userId = recuserIdlist(fromData: dataarray)
        }
        if let dataarray = data["providerId"] as? [String:Any]{
            self.providerId = providerIdlist(fromData: dataarray)
        }
    }
}

class categoryIdsList{
    var name : String?
    var categoryImage : String?
    init(fromData data: [String:Any]) {
        self.name = data["name"] as? String
        self.categoryImage = data["categoryImage"] as? String
    }
}

class recuserIdlist{
    var id : String?
    var fullName : String?
    var type : String?
    var categoryIds : [String]?
    var profilePic : String?
    var firstLevelConnections : [String]?
    var secondLevelConnections : [String]?
    var thirdLevelConnections : [String]?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.type = data["type"] as? String
        self.categoryIds = data["categoryIds"] as? [String]
        self.profilePic = data["profilePic"] as? String
        self.firstLevelConnections = data["firstLevelConnections"] as? [String]
        self.secondLevelConnections = data["secondLevelConnections"] as? [String]
        self.thirdLevelConnections = data["thirdLevelConnections"] as? [String]
    }
}

class providerIdlist{
    var fullName: String?
    var id : String?
    init(fromData data: [String:Any]) {
        self.fullName = data["fullName"] as? String
        self.id = data["_id"] as? String
    }
}

class consumeruserProfileDetailslist{
    var fullName : String?
    var profilePic : String?
    init(from data:[String:Any]){
        self.fullName = data["fullName"] as? String
        self.profilePic = data["profilePic"] as? String
    }
}
