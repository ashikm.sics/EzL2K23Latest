//
//  AddEditListModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/01/23.
//

import Foundation

class addGeneralResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class editGeneralResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class addProviderResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class editProviderResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class deleteResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class saveProfileImageResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class saveContactImageResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}

class uploadSingleImageResponse{
    var status : Bool?
    var filename : String?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.filename = data["filename"] as? String
        self.message = data["message"] as? String
    }
}

class tagProviderResponse{
    var status : Bool?
    var message : String?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
    }
}
