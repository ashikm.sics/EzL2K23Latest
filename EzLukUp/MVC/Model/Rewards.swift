//
//  File.swift
//  EzLukUp
//
//  Created by REMYA V P on 12/04/23.
//

import Foundation

class getRewardDescResponse{
    var status : Bool?
    var totalInvitePoints : Int?
    var totalTagPoints : Int?
    var Data : [getDataModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.totalInvitePoints = data["totalInvitePoints"] as? Int
        self.totalTagPoints = data["totalTagPoints"] as? Int
        if let datas = data["data"] as? [[String:Any]]{
            for dataarray in datas{
                self.Data.append(getDataModel(from: dataarray))
            }
        }
    }
}

class getDataModel{
    var id : String?
    var title : String?
    var description : String?
    var amount : Int?
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.title = data["title"] as? String
        self.description = data["description"] as? String
        self.amount = data["amount"] as? Int
   }
}

class getUserRewardDetailsResponse{
    var status : Bool?
    var Data : [getRewardDataModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datas = data["data"] as? [[String:Any]]{
            for dataarray in datas{
                self.Data.append(getRewardDataModel(from: dataarray))
            }
        }
    }
}

class getRewardDataModel{
    var id:  String?
    var userId: String?
    var invitedUserId : invitedUserIdlist?
    var rewardTypeId: rewardTypeIdlist?
    var amount: Int?
    var createdAt : String?
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.userId = data["userId"] as? String
        self.amount = data["amount"] as? Int
        self.createdAt = data["createdAt"] as? String
        if let invitearray = data["invitedUserId"] as? [String:Any]{
            self.invitedUserId = invitedUserIdlist(from: invitearray)
        }
        if let typearray = data["rewardTypeId"] as? [String:Any]{
            self.rewardTypeId = rewardTypeIdlist(from: typearray)
        }
   }

}

class invitedUserIdlist{
    var id : String?
    var fullName : String?
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
    }
}

class rewardTypeIdlist{
    var id : String?
    var rewardType : String?
    init(from data : [String:Any]){
        self.id = data["_id"] as? String
        self.rewardType = data["rewardType"] as? String
    }
}
