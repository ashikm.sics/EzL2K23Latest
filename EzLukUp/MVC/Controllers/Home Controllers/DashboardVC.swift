//
//  DashboardVC.swift
//  EzLukUp
//
//  Created by Srishti on 12/10/22.
//

import UIKit
import Alamofire
import Contacts
import ContactsUI

var checkdashcontacts = ""
var checkfeedback = ""
var checkprovidercont = ""
//var checkfeedback = ""

class DashboardVC: UIViewController {
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    var Dashboardmodel : DashboardDataModel?
    var userdetails : getUserDetailsResponse?
    var ConsumerDashboardmodel : ConsumerDashboardResponse?
    var checkdashboardtagcontacts : checkdashboardtagcontact?

//Outlets
    @IBOutlet weak var proDashTableview: UITableView!
    @IBOutlet weak var HelloLbl: UILabel!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var consumerView: UIView!
    @IBOutlet weak var consumerDashTableview: UITableView!
    @IBOutlet weak var providerView: UIView!
    @IBOutlet weak var tagContactView: BaseView!
    @IBOutlet weak var needTagContBlurView: UIView!

    
//Variables
    var phonenumber = ""
    var getcountrycode = ""
    var name = ""
    var location = ""
    
    let type : String = UserDefaults.standard.value(forKey: "Ktype") as! String
   
    let store = CNContactStore()
    var syncContact : Bool = false
    var contacDictionary = [[String:Any]]()
    var tempArr : [[String:Any]] = []
    let uploadCount = 1500
    var contactParam = [[String:Any]]()
    var uploadIsFaild = 3
    var activityView: UIActivityIndicatorView?
    var contactsCount = 0
    var timer = Timer()
    
    var getprofile = ""
    var getname = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
         
            self.setupUI()
            
//            self.dashboardAPI()
//            self.consumerdashapi()
           // self.tagContactView.isHidden = true
        }
        let username = UserDefaults.standard.value(forKey: "Kname")
//        HelloLbl.text = "Hello \(username ?? "")!"
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateProvider(notification:)), name: Notification.Name("nextslide"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigatedashapi(notification:)), name: Notification.Name("dashapi"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.syncapicalling(notification:)), name: Notification.Name("synchambergermenu"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // NotificationCenter.default.post(name: Notification.Name("imagename"), object: nil)
      //  self.setupUI()
    }
    
    @objc func navigatedashapi(notification: Notification) {
        if type == "Personal"{
            self.consumerView.isHidden = false
            self.providerView.isHidden = true
            self.consumerdashapi()
        }else{
            self.consumerView.isHidden = true
            self.providerView.isHidden = false
            self.dashboardAPI()
        }
    }
    
    @objc func navigateProvider(notification: Notification) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderProfileVC") as! ProviderProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setupUI(){
        if type == "Personal"{
            self.consumerView.isHidden = false
            self.providerView.isHidden = true
            self.consumerdashapi()
        }else{
            self.consumerView.isHidden = true
            self.providerView.isHidden = false
            self.dashboardAPI()
        }
    }
 
    @objc func syncapicalling(notification : Notification){
        DispatchQueue.main.async {
            self.requestAccess { accessGranted in
                print("contact request --",accessGranted)
                if accessGranted{
                    DispatchQueue.global(qos: .background).async {
                        self.getContactList()
                        DispatchQueue.main.async {
                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.autopush), userInfo: nil, repeats: false)
                        }
                    }
                }
            }
        }
    }
    
    func getContactList(){
        self.contacDictionary.removeAll()
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier:store.defaultContainerIdentifier())
        let contactz = try! store.unifiedContacts(matching: predicate, keysToFetch: [CNContactGivenNameKey as
                                                                                     CNKeyDescriptor,CNContactFamilyNameKey as CNKeyDescriptor ,CNContactPhoneNumbersKey as CNKeyDescriptor])
        contactsCount = contactz.count
        
        for contact in contactz {
            let name = "\(contact.givenName) \(contact.familyName)"
            var numbers: [String] = []
            for ph in contact.phoneNumbers{
                numbers.append(ph.value.stringValue)
            }
            let dictionaryObject: [String : Any] = [ "name": name, "phoneNumber": numbers]
            self.contacDictionary.append(dictionaryObject)
        }
      
        let sortedArray = contacDictionary.sorted { ($0["name"] as! String) < ($1["name"] as! String) }
        self.tempArr = sortedArray
            self.syncAPIParameter()
    }
    
    func syncAPIParameter() {
        self.uploadIsFaild = 3
        self.contactParam.removeAll()
        
        if self.tempArr.isEmpty { return }
        
        if self.tempArr.count <= self.uploadCount {
            self.contactParam = self.tempArr
            self.tempArr.removeAll()
        }
        else {
            self.contactParam = Array(self.tempArr.prefix(self.uploadCount))
            self.tempArr = Array(self.tempArr.dropFirst(self.uploadCount))
        }
        self.syncAPI(contacDictionary: self.contactParam)
    }
    
    
    //MARK: - Api call
    func syncAPI(contacDictionary: [[String:Any]] ){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        let countrycode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
            "userId": userid,
            "countryCode": countrycode,
            "syncdata": contacDictionary
        ]
        print("params",params)
        let url = kBaseUrl+"syncContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    syncContact = true
                    DispatchQueue.main.async {
                        self.syncAPIParameter()
                    }
                    
                    
                    print("responsedata :",responsedata)
                    if responsedata["message"] as? String == "Contact sync successfully"{
                        
                    }
                    else{
                        hideActivityIndicator()
                    }
                }
            case .failure(let error):
                if self.uploadIsFaild > 0 {
                    self.syncAPI(contacDictionary: self.contactParam)
                }
                self.uploadIsFaild -= 1
                showDefaultAlert(viewController: self, msg: "Request error: \(error.localizedDescription)")
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: - contact authorization
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            self.showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            self.store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    //MARK: - private alert for granting access again from settings
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
    @objc func autopush(){
        timer.invalidate()
    }
 
//    @IBAction func recFeedReportBTN(_ sender: UIButton) {
//        print(sender.tag)
//
//        UserDefaults.standard.setValue(self.ConsumerDashboardmodel?.Data?.recentFeedback[sender.tag].id, forKey: "Kcontactid")
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
////        vc.selecteduserid = self.ConsumerDashboardmodel?.Data?.recentFeedback.first?.id ?? ""
////        vc.getname = self.ConsumerDashboardmodel?.Data?.recentFeedback.first?.userId?.fullName ?? ""
//
//                self.navigationController?.pushViewController(vc, animated: true)
//    }
    
//    @IBAction func syncContBTNTapped(_ sender: UIButton) {
//        DispatchQueue.main.async {
//            self.requestAccess { accessGranted in
//                print("contact request --",accessGranted)
//               // if accessGranted{
//                    DispatchQueue.global(qos: .background).async {
//                        self.getContactList()
//                        DispatchQueue.main.async {
//                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.autopush), userInfo: nil, repeats: false)
//                        }
//                    }
//
//
//                //}
//
//            }
//
//        }
//    }
    
    
    
    @IBAction func tagContactBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func tagDismiss(_ sender: UIButton) {
        self.tagContactView.isHidden = true
        self.needTagContBlurView.isHidden = true
    }
    
    
    
    

    
}

//MARK: - @IBActions
extension DashboardVC{
    
    @IBAction func profileBTNTapped(_ sender: UIButton) {
        if type == "Personal"{
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ConsumerProfile") as! ConsumerProfile
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
 //MARK: -Consumer IBActions
    @IBAction func rewardsBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderRewards_VC") as! ProviderRewards_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func contactsBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func conViewAllContzBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
        checkdashcontacts = "contacts"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func recRecomProfileBTN(_ sender: UIButton) {
        print(sender.tag)

        print(self.ConsumerDashboardmodel?.Data?.recentRecommendProviders[sender.tag].id ?? "")
        UserDefaults.standard.setValue(self.ConsumerDashboardmodel?.Data?.recentRecommendProviders[sender.tag].id, forKey: "Kcontactid")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
        vc.checkrecomprofile = "fromprofile"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func consumerFeedBTN(_ sender: UIButton) {
        print(sender.tag)

        print(self.ConsumerDashboardmodel?.Data?.recentFeedback[sender.tag].providerId?.id ?? "")
        UserDefaults.standard.setValue(self.ConsumerDashboardmodel?.Data?.recentFeedback[sender.tag].providerId?.id, forKey: "KproviderId")

        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "DashboardProProfileVC") as! DashboardProProfileVC
        vc.getid = self.ConsumerDashboardmodel?.Data?.recentFeedback[sender.tag].providerId?.id ?? ""
       // vc.checkconsufeedback = "fromdata"
       // self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
 //MARK: -Provider IBActions
    @IBAction func proRewardsBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderRewards_VC") as! ProviderRewards_VC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func proStatsBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func prorecommendViewAll(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
        checkfeedback = "fromdata"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ProfeedReportViewAll(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
        checkfeedback = "fromdata"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func proRecomProfileBTN(_ sender: UIButton) {
        print(sender.tag)

        print(self.Dashboardmodel?.Data?.recentRecommendProviders[sender.tag].id ?? "")
        UserDefaults.standard.setValue(self.Dashboardmodel?.Data?.recentRecommendProviders[sender.tag].id, forKey: "Kcontactid")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func proFeddbackBTN(_ sender: UIButton) {
        print(sender.tag)
        print("bla")
     //   print(self.Dashboardmodel?.Data?.recentFeedback[sender.tag].providerId ?? "")
    //    UserDefaults.standard.setValue(self.Dashboardmodel?.Data?.recentFeedback[sender.tag].providerId, forKey: "Kuserid")
        
                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
                vc.checkfeedback = "fromdata"
               // vc.selectedcontactid = self.Dashboardmodel?.Data?.recentFeedback[sender.tag].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func proProfilevisitBTN(_ sender: UIButton) {
       // if Dashboardmodel?.Data?.
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
        checkprovidercont = "providercontacts"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


//MARK: - Tableview delegates
extension DashboardVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == consumerDashTableview{
        return 4
        }
        else{
            return 4
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == consumerDashTableview{
            if section == 0{
                //status section
                return 1
            }
            if section == 1{
                //recent recomendation
                return 1
                
            } else if section == 2{
                //recent feedback
                return 1
            }
            else {
                //joined contacts
                return 1
            }
        }
        else{
            if section == 0{
                //status section
                return 1
            }
            if section == 1{
                //recent recomendation
                return 1
            } else if section == 2{
                //recent feedback
                return 1
            }
            else {
                //
                return 1
            }
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == consumerDashTableview{
            if indexPath.section == 0 {
                let cell = consumerDashTableview.dequeueReusableCell(withIdentifier: "ConsumerListCell1", for: indexPath) as! ConsumerListCell1
                cell.totalRewardsCountLbl.text = "Total - \(ConsumerDashboardmodel?.Data?.rewardPoints?.description ?? "0") ezM"
                cell.contactCountLbl.text = ConsumerDashboardmodel?.Data?.inezContactsCount?.description ?? "0"
                cell.recommendCountLbl.text = ConsumerDashboardmodel?.Data?.recomentedProviderContactsCount?.description ?? "0"
                
                return cell
            }
            else if indexPath.section == 1 {
                let cell = consumerDashTableview.dequeueReusableCell(withIdentifier: "ConsumerListCell2", for: indexPath) as! ConsumerListCell2
                cell.ConsumerDashboardmodel = ConsumerDashboardmodel
               // cell.recommendCV.reloadData()
//                cell.recRecomEmptyview.isHidden = false
//                cell.recommendCV.isHidden = true
//
                if ConsumerDashboardmodel?.Data?.recentRecommendProviders.count ?? 0 != 0{
                    cell.recommendCV.isHidden = false
                    cell.recommendCV.reloadData()
                    cell.recRecomEmptyview.isHidden = true
                }else{
                    cell.recRecomEmptyview.isHidden = false
                    cell.recommendCV.isHidden = true
                }
                
                return cell
            }
            else if indexPath.section == 2 {
                let cell = consumerDashTableview.dequeueReusableCell(withIdentifier: "ConsumerListCell3", for: indexPath) as! ConsumerListCell3
                cell.ConsumerDashboardmodel = ConsumerDashboardmodel
                
                if ConsumerDashboardmodel?.Data?.recentFeedback.count ?? 0 != 0{
                    cell.feebbackReportCV.isHidden = false
                    cell.feebbackReportCV.reloadData()
                    cell.recEmptyview.isHidden = true
                }else{
                    cell.recEmptyview.isHidden = false
                    cell.feebbackReportCV.isHidden = true
                }
                
                return cell
            }
            else {
                let cell = consumerDashTableview.dequeueReusableCell(withIdentifier: "ConsumerListCell4", for: indexPath) as! ConsumerListCell4
                cell.ConsumerDashboardmodel = ConsumerDashboardmodel
                
                if ConsumerDashboardmodel?.Data?.recentlyJoinedUsers.count ?? 0 != 0{
                    cell.joinedContactsCV.isHidden = false
                    cell.joinedContactsCV.reloadData()
                    cell.recJointEmptyview.isHidden = true
                    cell.viewAllBTN.isHidden = false
                }else{
                    cell.recJointEmptyview.isHidden = false
                    cell.joinedContactsCV.isHidden = true
                    cell.viewAllBTN.isHidden = true

            }
                return cell
        }
        
        }
        else{
            if indexPath.section == 0 {
                let cell = proDashTableview.dequeueReusableCell(withIdentifier: "Listcell1", for: indexPath) as! Listcell1
                cell.rewardcountLBL.text = "Total - \(Dashboardmodel?.Data?.rewardPoints?.description ?? "0") ezM"
                cell.generalcontactscountLBL.text = Dashboardmodel?.Data?.inezContactsCount?.description ?? "0"
                cell.recommendingCntLBL.text = Dashboardmodel?.Data?.recomentingProviderContactsCount?.description ?? "0"
                cell.recommendationsCntLBL.text = Dashboardmodel?.Data?.recomentedProviderContactsCount?.description ?? "0"
                return cell
            }
            else if indexPath.section == 1 {
                let cell = proDashTableview.dequeueReusableCell(withIdentifier: "Listcell2", for: indexPath) as! Listcell2
                cell.Dashboardmodel = Dashboardmodel
                
                if Dashboardmodel?.Data?.recentRecommendProviders.count ?? 0 != 0{
                    cell.recomendationCV1.isHidden = false
                    cell.recomendationCV1.reloadData()
                    cell.emptyview.isHidden = true
                    cell.viewAllBTN.isHidden = false
                }else{
                    cell.emptyview.isHidden = false
                    cell.recomendationCV1.isHidden = true
                    cell.viewAllBTN.isHidden = true
                   // cell.viewAllBTN.isUserInteractionEnabled = false
                }
               // cell.recomendationCV1.reloadData()
                
                return cell
            }
            else if indexPath.section == 2 {
                let cell = proDashTableview.dequeueReusableCell(withIdentifier: "Listcell3", for: indexPath) as! Listcell3
                cell.Dashboardmodel = Dashboardmodel
                if Dashboardmodel?.Data?.recentFeedback.count ?? 0 != 0{
                    cell.recomendationCV2.isHidden = false
                    cell.recomendationCV2.reloadData()
                    cell.emptyview.isHidden = true
                    cell.viewAllBTN.isHidden = false
                }else{
                    cell.emptyview.isHidden = false
                    cell.recomendationCV2.isHidden = true
                    cell.viewAllBTN.isHidden = true
                }
               // cell.recomendationCV2.reloadData()
                return cell
            }
            else {
                let cell = proDashTableview.dequeueReusableCell(withIdentifier: "Listcell4", for: indexPath) as! Listcell4
                cell.Dashboardmodel = Dashboardmodel
                if Dashboardmodel?.Data?.profileVisitors.count ?? 0 != 0{
                    cell.recomendationCV3.isHidden = false
                    cell.recomendationCV3.reloadData()
                    cell.emptyview.isHidden = true
                }else{
                    cell.emptyview.isHidden = false
                    cell.recomendationCV3.isHidden = true
                }
               // cell.recomendationCV3.reloadData()
                return cell
            }
        }
        
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == consumerDashTableview{
            if indexPath.section == 0 {
                return UITableView.automaticDimension
            }
            else{
                return UITableView.automaticDimension
            }
        }
        else{
            if indexPath.section == 0 {
                return UITableView.automaticDimension
            }
//            else if indexPath.section == 1{
//                return UITableView.automaticDimension
//            }else if indexPath.section == 2{
//                return UITableView.automaticDimension
//            }
            else{
                return UITableView.automaticDimension
            }
        }
        
    }
    
    
}

extension DashboardVC{
    //MARK: - User details API Call
        func userdeatilsapi(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            
            let params = ["userId": userid] as [String : Any]
            
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            print("responsedata",responsedata)
                            self.userdetails = getUserDetailsResponse(from:responsedata)
                            let name = self.userdetails?.Data?.fullName ?? ""

                            let fullName = name
                                var components = fullName.components(separatedBy: " ")
                                if components.count > 0 {
                                 let firstName = components.removeFirst()
                                 let lastName = components.joined(separator: " ")
                                 debugPrint(firstName)
                                 debugPrint(lastName)
                               
                            self.HelloLbl.text = "Hello \( firstName)!"
                                }
                            
                            
                            self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                            
                           // self.infoTableView.reloadData()
                           // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                                 
                        }
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
    

    //MARK: - provider Dashboard API
    func dashboardAPI(){
        let params: [String : Any] = [
            "userId": userid]
        
        let urlp = kBaseUrl+"providerDashboardStatistics"
        let request =  AF.request(urlp, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    self.Dashboardmodel = DashboardDataModel(from:responsedata)
                    
                    self.HelloLbl.text = "Hello \(self.Dashboardmodel?.Data?.userProfileDetails?.fullName ?? "")!"

                    self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.Dashboardmodel?.Data?.userProfileDetails?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                    
                    DispatchQueue.main.async {
                        self.proDashTableview.reloadData()
                        
                    }
                    self.tagContactapi()
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    
//MARK: - consumer Dashboard API
func consumerdashapi(){
    
    let params = ["userId": userid] as [String : Any]

    let url = kBaseUrl+"dashboardStatistics"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.ConsumerDashboardmodel = ConsumerDashboardResponse(from:responsedata)
                    
                    self.HelloLbl.text = "Hello \( self.ConsumerDashboardmodel?.Data?.userProfileDetails?.fullName ?? "")!"

                    self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.ConsumerDashboardmodel?.Data?.userProfileDetails?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
                    
                    self.consumerDashTableview.reloadData()
                   
                    self.tagContactapi()
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
  }
    
  
 //MARK: - tag contact api call
    //MARK: - consumer Dashboard API
    func tagContactapi(){
        
        let params = ["userid": userid] as [String : Any]
       
        let url = kBaseUrl+"checkTagPopupVisibilityStatus"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.checkdashboardtagcontacts = checkdashboardtagcontact(from:responsedata)
    
                        if self.checkdashboardtagcontacts?.totalTaggedProviders == 0{
                            self.tagContactView.isHidden = false
                            self.needTagContBlurView.isHidden = false
                        }else{
                            self.tagContactView.isHidden = true
                            self.needTagContBlurView.isHidden = true
                        }
                             
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }
      }
    
}

extension DashboardVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}





























