//
//  SettingsVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 06/03/23.
//

import UIKit
import SafariServices

class SettingsVC: UIViewController,SFSafariViewControllerDelegate {

    
    @IBOutlet weak var versionLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
        versionLbl.text = "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")"
}

@objc func goToHomeTab(){
        navigationController?.popToRootViewController(animated: true)
    }
 
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
    }
    
    @IBAction func accsettingsBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "AccountSettingsVC") as! AccountSettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func accprivacyBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "AccountPrivacyVC") as! AccountPrivacyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacypolicyBTN(_ sender: UIButton) {
        let safariVC = SFSafariViewController(url: URL(string: "http://ezlukup.com/privacy-policy/")!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
    
    @IBAction func termsofuseBTN(_ sender: UIButton) {
        let safariVC = SFSafariViewController(url: URL(string: "http://ezlukup.com/terms-of-service/")!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
//        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "TermsOfUseVC") as! TermsOfUseVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signoutBTN(_ sender: UIButton) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav = UINavigationController(rootViewController: homeViewController)
                nav.navigationBar.isHidden = true
                appdelegate.window!.rootViewController = nav
        do {
            try  DatabaseGeneralContactEntity().deleteAllRecords(fromCoreData: "GeneralListData", context: DatabaseGeneralContactEntity().mainObjectContext)
        } catch let error as NSError {
            print("Error deleting records: \(error.localizedDescription)")
        }
        do {
            try  DatabaseGeneralContactEntity().deleteAllRecords(fromCoreData: "GcontactUserID", context: DatabaseGeneralContactEntity().mainObjectContext)
        } catch let error as NSError {
            print("Error deleting records: \(error.localizedDescription)")
        }
        do {
            try DatabaseGeneralContactEntity().deleteAllRecords(fromCoreData: "ProviderCategoryEntity", context: DatabaseGeneralContactEntity().mainObjectContext)
        } catch let error as NSError {
            print("Error deleting records: \(error.localizedDescription)")
        }
        do {
            try DatabaseGeneralContactEntity().deleteAllRecords(fromCoreData: "ProviderContactsEntity", context: DatabaseGeneralContactEntity().mainObjectContext)
        } catch let error as NSError {
            print("Error deleting records: \(error.localizedDescription)")
        }

                UserDefaults.standard.setValue("false", forKey: "loggedin")
                        navigationController?.popToRootViewController(animated: true)
                        loggedin = false
                        alreadyloggedin = false
                        editproviderpopupstatuscheck = false
    }

    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }


}
