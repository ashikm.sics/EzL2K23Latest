//
//  ContactAlertVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 24/10/22.
//

import UIKit

class ContactAlertVC: UIViewController {

    @IBOutlet weak var continueBtn: BaseButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnContinueAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "PageViewController") as! PageViewController
        self.navigationController?.pushViewController(vc, animated: true)
//        self.navigationController?.popViewController(animated: true)
        
    }
    

}
