//
//  DifferentDeviceVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 08/08/23.
//

import UIKit
import Alamofire

class DifferentDeviceVC: UIViewController {

    
    @IBOutlet weak var continueBTN: UIButton!
    @IBOutlet weak var exitBTN: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        exitBTN.backgroundColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
      
    }
    
    
    @IBAction func exitBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "WelcomeVC") as! WelcomeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func continueBTNTapped(_ sender: Any) {
        updatedevicedetails()
    }
    

    //MARK: - different device checking API
    func updatedevicedetails(){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        let id = UIDevice.current.identifierForVendor?.uuidString ?? ""
                print("idddd",id)
        let name = UIDevice.current.model
        print("nameee",name)
        let osversion = UIDevice.current.systemVersion
        print("osversionn",osversion)
        let phonetype = UIDevice.current.systemName
        print("phonetypee",phonetype)
        let phonemake2 = UIDevice.current.name
        print("phonetypee",name)
        let params: [String : Any] = [
                      "userId": userid,
                      "phoneMake": name ,
                      "osVersion": osversion,
                      "phoneType": phonetype,
                      "deviceToken": id
         ]
        print(params)

        let url = kBaseUrl+"updateDeviceToken"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata :",responsedata)
                        
                        if responsedata["status"] as! Bool == true {
                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
                            let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                      }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }
      }
   

}

//
//"id": "64d2131c928891fd19eb90d3",
//    "isrecommended": true,
//    "feedback": "very hardworking person14",
//    "categoryIds": ["64748178bd5085ea19ae3ad8"]
//64d211fc928891fd19eb6d35
