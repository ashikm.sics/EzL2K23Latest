//
//  SlidingVC.swift
//  EzLukUp
//
//  Created by Srishti on 03/11/22.
//

import UIKit

class SlidingVC: UIViewController {
//variables & declaration
    var imgarray = [""]
    var subtextarray = ["In order to access the network we need to sync contacts so that you can search for Providers across your connections.You will be able to Serach and also navigate to your contacts using ezlukup to see their recommendations.","Search your contacts for Providers and and Tag them as Providers by swiping to the right.Recommend the providers who you believe will provide the right value to your connections.","Search for Providers across your different levels of connections.Reach out to them for your service needs","ezlukup rewards you for your efforts for : Inviting your contacts Recommeding Providers Answering questions Collaborate and make the platform useful for you and your connections"]
    let t1 = ""
    let t2 = ""
    let t3 = ""
    let t4 = ""
    var headtextarray = [""]
    var currentIndex = 0
//Outlets
    
    @IBOutlet weak var SliderCV: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //actions
    
    @IBAction func ContinueBTN(_ sender: UIButton) {
        print(sender.tag)
        if sender.tag < subtextarray.count - 1 {
           
            self.SliderCV.scrollToItem(at:IndexPath(item: sender.tag + 1, section: 0), at: .right, animated: false)
        }
        else{
            print("going to next page")
        }
        
    }
    
}

extension SlidingVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subtextarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = SliderCV.dequeueReusableCell(withReuseIdentifier: "slidercell", for: indexPath) as! slidercell
        cell.SlidersubLBL.text = subtextarray[indexPath.row]
        cell.ContinueBTN.tag = indexPath.row
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = SliderCV.frame.height
        let w = (SliderCV.frame.width) / 1
        return CGSize(width: w, height: h)
    }
    
}




    //cell class
class slidercell:UICollectionViewCell{
    @IBOutlet weak var SliderImg: UIImageView!
    @IBOutlet weak var SliderheadLBL: UILabel!
    @IBOutlet weak var SlidersubLBL: UILabel!
    
    @IBOutlet weak var ContinueBTN: UIButton!
}
