//
//  ProviderEditMyProfileVC.swift
//  EzLukUp
//
//  Created by mac on 20/06/23.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI


class ProviderEditMyProfileVC: UIViewController {
    
    var PMPEuserdetails : getUserDetailsResponse?
    var PMPEsearchArrRes = [getcatDataModel]()
    var PMPEnewcatarray = [getcatDataModel]()
    var PMPEcategorylist : getCategoriesResponse?
    var PMPEservices = [SerViceModel]()
    var PMPEcityarray = [[String:Any]]()
    var PMPEstatearray = [[String:Any]]()
    var PMPEstateListModel : SearchStateList?
    var PMPEstateListArray = [StateList]()
    var PMPESearchstateList = [StateList]()
    var PMPEcityList : SearchCityList?
    var PMPEcityListArray = [CityList]()
    var PMPESearchcityList = [CityList]()
    var servicesArray = [String]()
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var areaOfServiceView: UIView!
    @IBOutlet weak var serviceView: UIView!
    @IBOutlet weak var serviceListView: UIView!
    
    @IBOutlet weak var categoriesView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var closeCategoriesButton: UIButton!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var stateTBL: UITableView!
    @IBOutlet weak var cityTBL: UITableView!
    @IBOutlet weak var radiusTBL: UITableView!
    @IBOutlet weak var servicesTableView: UITableView!
    @IBOutlet weak var servicesTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var searchIMG: UIImageView!
    @IBOutlet weak var categoriesTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var companyNameTF: UITextField!
    @IBOutlet weak var operationsTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var stateDropview: UIView!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var cityDropview: UIView!
    @IBOutlet weak var detailsTxtView: UITextView!

    @IBOutlet weak var radiusTF: UITextField!
    @IBOutlet weak var dropIMG: UIImageView!
    @IBOutlet weak var radiusview: UIView!
    @IBOutlet weak var radiusdropView: UIView!
    
    
    var radiusArray = ["25","50","75","100"]
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    var latitude = "38.00000000"
    var longitude = "-97.00000000"
    var state = ""
    var city = ""
    var mobnumber = ""
    var name = ""
    var companyname = ""
    var operationalhr = ""
    var about = ""
    var radius = "25 miles"
    
    var dropdownClicked = false
    
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var imageupload = ""
    var activityView: UIActivityIndicatorView?
    
    var EPSselectedcategoryID = [String]()
    var editCatID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCategoryAPI()
        self.getstateAPI()
        self.getProviderProfileDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHomeTab), name: Notification.Name("hometab"), object: nil)
    }
    
    @objc func goToHomeTab(){
            navigationController?.popToRootViewController(animated: true)
        }
    
    @IBAction func radiusdropBTNAction(_ sender: UIButton) {
        self.radiusdropView.isHidden = !self.radiusdropView.isHidden
        
//        if dropdownClicked{
//            self.radiusdropView.isHidden = false
//            UIView.animate(withDuration: 0, animations: {
//                self.dropIMG.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
//            })
//
//        }else{
//            self.radiusdropView.isHidden = true
//            UIView.animate(withDuration: 0, animations: {
//                 self.dropIMG.transform = CGAffineTransform.identity
//            })
//        }
        
    }

    @IBAction func infoBTNAction(_ sender: UIButton) {
        self.infoView.isHidden = !self.infoView.isHidden
    }
    @IBAction func areaOfServiceBTNAction(_ sender: UIButton) {
        self.areaOfServiceView.isHidden = !self.areaOfServiceView.isHidden
    }
    @IBAction func serviceBTNAction(_ sender: UIButton) {
        self.serviceView.isHidden = !self.serviceView.isHidden
    }
    
    @IBAction func addServiceBTNAction(_ sender: UIButton) {
        if self.checkForEmptyService() {
           // self.servicesArray.append(SerViceModel(name: ""))
            self.serviceListView.isHidden = false
            self.PMPEservices.append(SerViceModel(name: ""))
          //  self.servicesArray = self.PMPEservices
            self.servicesTableView.reloadData()
            if PMPEservices.count == 1{
                self.servicesTableViewHeight.constant = 55
            }
            else {
                self.servicesTableViewHeight.constant = self.servicesTableView.contentSize.height
            }
        
        } else {
            self.showToast(message: "Enter Service", font: .systemFont(ofSize: 13.0))
        }
    }
    @IBAction func removeCategorySelectionBTNAction(_ sender:UIButton){
            self.categoryLabel.isHidden = true
            self.categoryLabel.text = ""
            self.categoriesTF.text = ""
            self.closeCategoriesButton.isHidden = true
            self.categoriesTF.isHidden = false
            self.searchIMG.isHidden = false
        
    }
    @IBAction func backBTNAction(_ sender: UIButton) {
      //  self.navigationController?.popViewController(animated: true)
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
                self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func closeBTNAction(_ sender:UIButton){
//        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
//                self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    func checkForEmptyService() -> Bool {
        return self.PMPEservices.filter({ !($0.name == "") }).count == self.PMPEservices.count
    }
    
    @IBAction func SaveBTNAction(_ sender: UIButton) {
        saveProviderProfileDetails()
    }
    
    @IBAction func ServiceCloseBTNAction(_ sender: UIButton) {
        print("index -",sender.tag)
        print("service selected = ",PMPEservices[sender.tag].name)
        PMPEservices.remove(at: sender.tag)
        servicesTableView.reloadData()
        if PMPEservices.count == 1{
            self.servicesTableViewHeight.constant = 55
        }
        else {
            self.servicesTableViewHeight.constant = self.servicesTableView.contentSize.height
        }
        
    }
    
}


//MARK: -UITextFieldDelegates & UITextViewDelegate
extension ProviderEditMyProfileVC: UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("area of service")
        
        if textField == stateTF{
            if checkareastatus == false{
                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "AreaOfServiceMsgVC") as! AreaOfServiceMsgVC
                //           checkareastatus = false
                view.endEditing(true)
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
            }
            
        }
        else if textField == cityTF{
            if checkareastatus == false{
                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "AreaOfServiceMsgVC") as! AreaOfServiceMsgVC
                //           checkareastatus = false
                view.endEditing(true)
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == categoriesTF {
            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.categoriesView.isHidden = newText == ""
            
            self.PMPEsearchArrRes = newText == "" ? self.PMPEnewcatarray : self.PMPEnewcatarray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.categoryTableView.reloadData()

        }
        else if textField == stateTF{
            
            PMPEcityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.radiusview.isHidden = true
            cityTBL.reloadData()
            
            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.stateDropview.isHidden = newText == ""
            
            self.PMPESearchstateList = newText == "" ? self.PMPEstateListArray :
            self.PMPEstateListArray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.stateTBL.reloadData()
            
               

        }
        else if textField == cityTF{

            guard let text = textField.text else { return true }
                let newText = (text as NSString).replacingCharacters(in: range, with: string)
                self.cityDropview.isHidden = newText == ""
                
                self.PMPESearchcityList = newText == "" ? self.PMPEcityListArray :
                self.PMPEcityListArray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
                self.cityTBL.reloadData()
            
            self.radiusview.isHidden = false
            self.radiusTF.text = "25 miles"
            
            if self.cityTF.text?.count == 1{
                self.radiusview.isHidden = true
                self.radiusTF.text?.removeAll()

            }
            
            
           
        }
        return true
    }
}


//MARK: -UITableViewDelegates
extension ProviderEditMyProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.categoryTableView {
            return self.PMPEsearchArrRes.count
        } else if tableView == self.stateTBL{
            return PMPESearchstateList.count
        } else if tableView == self.cityTBL{
            return PMPESearchcityList.count
        } else if tableView == self.radiusTBL{
            return radiusArray.count
        } else {
            return self.PMPEservices.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.categoryTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: PEMcategorytvcell.identifier) as! PEMcategorytvcell
            cell.categoryLbl.text = self.PMPEsearchArrRes[indexPath.row].name
            return cell
        } else if tableView == self.stateTBL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "statecell") as! statecell
            cell.statenameLBL.text = PMPESearchstateList[indexPath.row].name
           // cell.statenameLBL.text = PMPEstatearray[indexPath.row] as? String
            return cell
        } else if tableView == self.cityTBL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "citycell") as! citycell
            cell.citynameLBL.text = PMPESearchcityList[indexPath.row].name
            return cell
        } else if tableView == self.radiusTBL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "radiuscell") as! radiuscell
            cell.radiusLBL.text = radiusArray[indexPath.row] + " miles"
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: serviceTableviewcell.identifier) as! serviceTableviewcell
            cell.closeBTN.tag = indexPath.row
            cell.serviceTF.text = self.PMPEservices[indexPath.row].name
            cell.endEditingCallBack { text in
                self.PMPEservices[indexPath.row].name = text
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView {
            self.categoryLabel.isHidden = false
            self.closeCategoriesButton.isHidden = false
            self.categoriesTF.isHidden = true
            self.searchIMG.isHidden = true
            self.categoriesView.isHidden = true
            self.categoryLabel.text = self.PMPEsearchArrRes[indexPath.row].name
            editCatID = self.PMPEsearchArrRes[indexPath.row].id ?? ""
            EPSselectedcategoryID.append(editCatID)
        } else if tableView == stateTBL{
            let stName = self.PMPESearchstateList[indexPath.row].name ?? ""
            stateTF.text = stName
           
            let index = PMPEstateListArray.firstIndex(where: { ($0.name) == stName }) ?? 0
            self.state = PMPEstateListModel?.Data[index].isoCode ?? ""
            print(self.PMPEstateListModel?.Data[index].isoCode ?? "")

            self.PMPEcityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.cityTBL.reloadData()
            self.radiusview.isHidden = true
            getcityAPI(stateiso: PMPEstateListModel?.Data[index].isoCode ?? "")
            self.stateDropview.isHidden = true
            
            self.latitude = self.PMPESearchstateList[indexPath.row].latitude ?? ""
            self.longitude = self.PMPESearchstateList[indexPath.row].longitude ?? ""
            
            print(self.PMPESearchstateList[indexPath.row].latitude ?? "")
            print(self.PMPESearchstateList[indexPath.row].longitude ?? "")
            
        } else if tableView == cityTBL {
            let ctName = self.PMPESearchcityList[indexPath.row].name ?? ""
            cityTF.text = ctName
            self.city = self.PMPESearchcityList[indexPath.row].name ?? ""
            self.cityDropview.isHidden = true
            
            self.latitude = PMPESearchcityList[indexPath.row].latitude ?? ""
            self.longitude = PMPESearchcityList[indexPath.row].longitude ?? ""
            
            self.radiusview.isHidden = false
            self.radiusTF.text = "25 miles"
            
        }  else if tableView == radiusTBL {
            self.radiusTF.text = radiusArray[indexPath.row] + " miles"
            radius = radiusArray[indexPath.row]
            self.radiusdropView.isHidden = true
            
        }
        }
        
    }


//MARK: - UIImagePickerControllerDelegates & functions
extension ProviderEditMyProfileVC:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
    
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
   
    
    @IBAction func imgPickerBTN(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
           }
       
    //MARK: image picker delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.editedImage] as! UIImage

        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        userImage.image = pickedimage
        print("image ==== ",pickedimage as Any)

        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
        
        if pickedimage != nil{
            uploadImage()
        }
    }
    
    
    //MARK: - PROFILE UPLOAD API CALL
         func uploadImage(){
             if selectedProfileImage != nil{
                 //showActivityIndicator()
                 let uiImage : UIImage = self.selectedProfileImage
                 imageData = uiImage.jpegData(compressionQuality: 0.2)!
                 
                 imageStr = imageData.base64EncodedString()
                 fileName = "image"
                 mimetype = mimeType(for: imageData)
                 print("mime type = \(mimetype)")
             }else{
                 imageData = Data()
                 fileName = ""
             }
           
             ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
                 print(response as Any)
                 if success {
                     if response!["status"] as! Bool == true {
                        // self.hideActivityIndicator()
                         print("image upload success")
                         
                     }else {
                         showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                     }
                 }else{
                     print(error?.localizedDescription as Any)
                     showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
                 }
             }
         }
}

extension ProviderEditMyProfileVC {
    
    //MARK: - category api
    func getCategoryAPI() {
    
        let url = kBaseUrl+"getCategories"
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
            print(response)
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    self.PMPEcategorylist = getCategoriesResponse(from: responsedata)
                    self.PMPEnewcatarray = self.PMPEcategorylist?.Data ?? []
                    DispatchQueue.main.async {
                        self.PMPEsearchArrRes = self.PMPEnewcatarray
                        self.categoryTableView.reloadData()
                    }
                }
                
                
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: - User details API Call
    func getProviderProfileDetails(){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userId": userid] as [String : Any]
        
        let url = kBaseUrl+"getUserDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print("Edit profile datas--",response)
            switch (response.result) {
                
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    self.PMPEuserdetails = getUserDetailsResponse(from:responsedata)
                    self.userImage.kf.setImage(with: URL(string: kImageUrl + (self.PMPEuserdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "addprofile"))

                    self.nameTF.text = self.PMPEuserdetails?.Data?.fullName ?? ""
                    if self.PMPEuserdetails?.Data?.categoryIds.first?.name != nil{
                        self.closeCategoriesButton.isHidden = false
                        self.categoriesTF.isHidden = true
                        self.searchIMG.isHidden = true
                        self.categoriesView.isHidden = true
                        self.categoryLabel.isHidden = false
                        self.categoryLabel.text = self.PMPEuserdetails?.Data?.categoryIds.first?.name
                        self.editCatID = self.PMPEuserdetails?.Data?.categoryIds.first?.id ?? ""
                        self.EPSselectedcategoryID.append(self.editCatID)
                    }
                    else{
                        self.categoryLabel.text = ""
                        self.categoryLabel.isHidden = true
                    }
//                    self.operationsTF.text = self.userdetails?.Data.
                    self.phoneTF.text = self.PMPEuserdetails?.Data?.phoneNumber ?? ""
                    self.companyNameTF.text = self.PMPEuserdetails?.Data?.companyName ?? ""
                    self.operationsTF.text = self.PMPEuserdetails?.Data?.operationalHours ?? ""
                    self.detailsTxtView.text = self.PMPEuserdetails?.Data?.aboutBusiness ?? ""
                    
                    if self.PMPEuserdetails?.Data?.areaOfService.count ?? 0 > 0 && ((self.PMPEuserdetails?.Data?.areaOfService.first?.stateShortCode?.isEmpty) != nil){
                        print("bla")
                        print(self.PMPEuserdetails?.Data?.areaOfService.first?.stateShortCode ?? "")

                        if self.PMPEstateListArray.count > 0{
                            for item in self.PMPEstateListArray {
                                print(item)
                                print("chakka")
                                if ((item.isoCode?.contains(self.PMPEuserdetails?.Data?.areaOfService.first?.stateShortCode ?? "")) == true){

                                    self.stateTF.text = item.name
                                 //  self.state = item.isoCode ?? ""
                                }
                            }

                        }
                        else
                        {
                           // self.stateTF.text = self.PMPEuserdetails?.Data?.areaOfService.first?.stateShortCode
                            self.state = self.PMPEuserdetails?.Data?.areaOfService.first?.stateShortCode ?? ""
                        }
                    }
                    
                   
                    self.cityTF.text = self.PMPEuserdetails?.Data?.areaOfService.first?.city ?? ""
                    self.radiusTF.text = "\(self.PMPEuserdetails?.Data?.areaOfService.first?.radius ?? "") miles"
        
                    self.servicesArray = self.PMPEuserdetails?.Data?.serviceDetails ?? []
                    
                    
                    self.PMPEservices = self.servicesArray.compactMap({SerViceModel(name: $0)})
                    
                    self.serviceListView.isHidden = self.PMPEservices.count == 0
                   
                    self.servicesTableView.reloadData()
                   // DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                    self.servicesTableViewHeight.constant = CGFloat(self.PMPEservices.count * 62)
                //    })
                    
                    self.city = self.PMPEuserdetails?.Data?.areaOfService.first?.city ?? ""
                    self.radius = self.PMPEuserdetails?.Data?.areaOfService.first?.radius ?? ""
                    self.name = self.PMPEuserdetails?.Data?.fullName ?? ""
                    self.mobnumber = self.PMPEuserdetails?.Data?.phoneNumber ?? ""
                    self.about = self.PMPEuserdetails?.Data?.aboutBusiness ?? ""
                    self.companyname = self.PMPEuserdetails?.Data?.companyName ?? ""
                    self.operationalhr = self.PMPEuserdetails?.Data?.operationalHours ?? ""
                    self.latitude = self.PMPEuserdetails?.Data?.latitude ?? ""
                    self.longitude = self.PMPEuserdetails?.Data?.longitude ?? ""
                    
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            
            
        }
    }
    
    
    
    func getstateAPI(){
        // showActivityIndicator()
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
             
             let params: [String : Any] = [
                           "country_iso_code": "US"]
         
             let url = kBaseUrl+"statesList"
         AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             print(response)
              switch (response.result)      {
                  
                case .success( let JSON):
                  
                  if let responsedata =  JSON as? [String:Any]  {
                      print("responsedata",responsedata)
                      self.PMPEstateListModel = SearchStateList(from:responsedata)
                      self.PMPEstateListArray = self.PMPEstateListModel?.Data ?? []
                      if let dataa = responsedata["data"] as? [[String:Any]]{
     //                        print("dataa = ", dataa)
                         
                          for i in dataa{
                              print("looped data =",i)
                              self.PMPEstatearray.append(i)
                          }
                      }
                      self.stateTBL.reloadData()
                  }
                      
                     
                           
                
              case .failure(let error):
                  print("Request error: \(error.localizedDescription)")
              }
          }
       }
    
    func getcityAPI(stateiso:String){
        //   showActivityIndicator()
               let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
               let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
               
               let params: [String : Any] = [
                             "country_iso_code": "US",
                             "state_iso_code": stateiso]
           
           // let url = "http://13.234.177.61/api7/citiesList"
               let url = kBaseUrl+"citiesList"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
                switch (response.result)      {
                  case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.PMPEcityList = SearchCityList(from:responsedata)
                        self.PMPEcityListArray = self.PMPEcityList?.Data ?? []
                        if let dataa = responsedata["data"] as? [[String:Any]]{

                            for i in dataa{
                                print("looped data =",i)
                                self.PMPEcityarray.append(i)
                            }
                        }
                    }
                    
                        self.cityTBL.reloadData()
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            }
       }
    

//MARK: - Save User details API Call
    func saveProviderProfileDetails(){
        
        //self.state = self.stateTF.text ?? ""
       // self.city = self.cityTF.text ?? ""
        
        self.name = self.nameTF.text ?? ""
        self.mobnumber = self.phoneTF.text ?? ""
        self.about = self.detailsTxtView.text ?? ""
        self.companyname = self.companyNameTF.text ?? ""
        self.operationalhr = self.operationsTF.text ?? ""
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["userid": userid,
                      "name": name,
                      "countrycode": "+1",
                      "phonenumber": mobnumber,
                      "state": state,
                      "city": city,
                      "country": "us",
                      "radiusOfService": radius,
                      "latitude": latitude,
                      "longitude": longitude,
                      "companyname": companyname,
                      "operationalhours": operationalhr,
                      "aboutbusiness": about,
                      "categoryids": EPSselectedcategoryID,
                      "servicedetails": PMPEservices.compactMap({ $0.name})
                      ] as [String : Any]
        
        print(params)
        let url = kBaseUrl+"updateUserDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print("Edit profile datas--",response)
            switch (response.result) {
                
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
      
                      let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                      let vc = storyboard.instantiateViewController(identifier: "ProviderMyProfile") as! ProviderMyProfile
                              self.navigationController?.pushViewController(vc, animated: true)
                  }
                
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            
            
        }
    }
}



struct SerViceModel {
    var name: String = ""
}


//MARK: - Cell classes
class PEMcategorytvcell : UITableViewCell{
    static let identifier = String(describing: PEMcategorytvcell.self)

    @IBOutlet weak var categoryLbl: UILabel!
}

class serviceTableviewcell:UITableViewCell, UITextFieldDelegate {
    
    static let identifier = String(describing: serviceTableviewcell.self)
    var endEditingCompletion:((String)->())? = nil
    @IBOutlet weak var serviceTF: UITextField!
    @IBOutlet weak var closeBTN: UIButton!

    func endEditingCallBack(_ completion: @escaping(String)->()) {
        self.endEditingCompletion = completion
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.endEditingCompletion?(textField.text ?? "")
        return true
    }
}

class statecell : UITableViewCell{
    @IBOutlet weak var statenameLBL:UILabel!
}

class citycell: UITableViewCell{
    @IBOutlet weak var citynameLBL: UILabel!
    
}

class radiuscell: UITableViewCell{
    @IBOutlet weak var radiusLBL: UILabel!
    
}
