//
//  ProviderMyProfileEditVC.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 05/04/23.
//

import UIKit
import CoreLocation
import Alamofire


var statedropstatusForProviderProviderprofileEdit = false
var citydropstatusForProviderProviderprofileEdit = false
var radiusdropstatusForProviderProviderprofileEdit = false

class ProviderMyProfileEditVC: UIViewController,deletearraydelegate {

    
    func firtcell(cell: Firstcell, searchtext: String) {
        showResults(string: searchtext)
        pname = cell.nameTextField.text ?? ""
        psearchvlaue = searchtext
        cell.locationTextField.becomeFirstResponder()
    }
    func firtcell(cell:Firstcell,didselect item :String){
        pname = cell.nameTextField.text ?? ""
        psearchvlaue = item
        self.autocompleteResults.removeAll()
        ProviderProfileEditTableView.reloadData()
    }
    func diddelete(index: Int) {
        let newindex = newcatarray.firstIndex{$0.name == selectedcategories[index]}
        newcatarray[newindex ?? 0].isselected = false
        selectedcategories.remove(at: index)
        categoryTableView.reloadData()
        ProviderProfileEditTableView.reloadData()
        
    }
    
    @IBOutlet weak var ProviderProfileEditTableView : UITableView!
    @IBOutlet weak var categoryTableView: UITableView!
    //locationpop
    @IBOutlet var locationPopupView: UIView!
    @IBOutlet weak var poplocationTextfield:UITextField!
    @IBOutlet var locationTableview: UITableView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var categoryPopupView: UIView!
    @IBOutlet weak var catSearchTF: UITextField!
    
  
   
    @IBOutlet weak var TF_State:UITextField!
    @IBOutlet weak var TF_City:UITextField!
    
    var userdetails : getUserDetailsResponse?
    var imagePicker = UIImagePickerController()
    var blurView : UIView!
    let kScreenWidth = UIScreen.main.bounds.width
    let kScreenHeight = UIScreen.main.bounds.height
    var isLocationHasValue = false
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var selectedLocationFromPopup = ""
    var infoClicked = false
    var areaOfServiceClicked = false
    var dropstatus = false
 

    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var psearchvlaue = ""
    var pname = ""
    let locationManager = CLLocationManager()
    var getlocality = ""
    var getcountry = ""
    var fulllocation = ""
    var getlat = ""
    var getlong = ""
    var Uname = ""
    var mob = ""
    var pickedimage :UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategoryAPI()
        getProviderProfileDetails()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
         statedropstatusForProviderProviderprofileEdit = false
         citydropstatusForProviderProviderprofileEdit = false
         radiusdropstatusForProviderProviderprofileEdit = false
    }

    @IBAction func backBTNAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeBTNAction(_ sender:UIButton){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnDropdownAction(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.ProviderProfileEditTableView.reloadSections([2], with: .none)
    }
    
    @objc func exapndInfo(){
        print("drop down button clicked")
        infoClicked = !infoClicked
        if infoClicked{
            infoClicked = true
            ProviderProfileEditTableView.reloadData()
        }else{
            infoClicked = false
            ProviderProfileEditTableView.reloadData()
        }
        
    }
    
    @objc func exapndAreaofServices(){
        print("drop down button clicked")
        areaOfServiceClicked = !areaOfServiceClicked
        if areaOfServiceClicked{
            areaOfServiceClicked = true
            ProviderProfileEditTableView.reloadData()
        }else{
            areaOfServiceClicked = false
            ProviderProfileEditTableView.reloadData()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            print("location manager authorization status changed")
            
            switch status {
            case .authorizedAlways:
                print("user allow app to get location data when app is active or in background")
            case .authorizedWhenInUse:
                print("user allow app to get location data only when app is active")
            case .denied:
                print("user tap 'disallow' on the permission dialog, cant get location data")
            case .restricted:
                print("parental control setting disallow location data")
            case .notDetermined:
                print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
            }
        }

}


extension ProviderMyProfileEditVC: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ProviderProfileEditTableView{
            return 1
        }else{
            //check search text & original text
            if( searching == true){
                return searchArrRes.count
            }else{
                return newcatarray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ProviderProfileEditTableView{
            if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderProfileEditTVCell", for: indexPath) as! ProviderProfileEditTVCell
                cell.btnDropDownInfo.addTarget(self, action: #selector(exapndInfo), for: .touchUpInside)
                return cell
            }else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderProfileEditSecondCell", for: indexPath) as! ProviderProfileEditSecondCell
                cell.profileImage.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profile icon"))
                cell.nameTextfield.text = self.userdetails?.Data?.fullName ?? ""
                return cell
            }else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "SecondCategorycell", for: indexPath) as! SecondCategorycell
                cell.delegate = self
                cell.selectedArray = selectedcategories
                cell.CategoryTV.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    if self.dropstatus {
                       
                        cell.searchView.isHidden = false
                        cell.searchViewHeight.constant = 40
                        cell.dropviewHeight.constant = 500
                        cell.dropview.isHidden = false
                        cell.dropimg.image = UIImage(named: "dropup_icon")
                        cell.dropview.addSubview(self.categoryPopupView)
                       
                    }
                    else{
                        
                        cell.dropviewHeight.constant = 0
                        cell.searchViewHeight.constant = 0
                        cell.searchView.isHidden = true
                        cell.dropview.isHidden = true
                        cell.dropimg.image = UIImage(named: "dropdown_icon")
                       
                    }
                    if selectedcategories.count == 1{
                        if self.dropstatus {
                            cell.catgoryviewHeight.constant = 83

                        }else {
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 11 )
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11 )
                        }
                    }
                    else if selectedcategories.count > 1{
                        if self.dropstatus{
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 40 + 11)
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 40 + 11)
                        }
                        else{
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 11)
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11)
                        }
                    }
                        else{
                            
                            cell.catgoryviewHeight.constant = 52
                            
                        }
                    
                }
                )
                
                return cell
            }else if indexPath.section == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderProfileEditFourthCell", for: indexPath) as! ProviderProfileEditFourthCell
                cell.TF_Phone.text = self.userdetails?.Data?.phoneNumber ?? ""
                cell.TF_CompanyName.text = ""
                cell.TF_HOP.text = ""
                cell.descriptionTxt.text = self.userdetails?.Data?.aboutBusiness ?? ""
                return cell
            }else if indexPath.section == 4{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderProfileEditAreaofServicesHeaderTVCell", for: indexPath) as! ProviderProfileEditAreaofServicesHeaderTVCell
                cell.btnDropDownInfo.addTarget(self, action: #selector(exapndAreaofServices), for: .touchUpInside)
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderProfileEditLocationCell", for: indexPath) as! ProviderProfileEditLocationCell
                
                cell.callback = {
                    self.ProviderProfileEditTableView.reloadData()//
                    if statedropstatusForProviderProviderprofileEdit {
                            
                            cell.stateHeightView.constant = 500
    //                                            self.setupTableView.reloadData()
                        }
                    
                    }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                print(statedropstatusForProviderProviderprofileEdit)
                if statedropstatusForProviderProviderprofileEdit {
                        
                        cell.stateHeightView.constant = 500
//                                            self.setupTableView.reloadData()
                    }
                    else{
                        //  self.setupTableView.reloadData()
                        cell.stateHeightView.constant = 0
                        
                    }
                })
                
                return cell
            }
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryPopupCell") as! CategoryPopupCell
            print("status",searching)
            cell.catLbl.text = searchArrRes[indexPath.row].name ?? "" //
            cell.catLbl.textColor = searchArrRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
            return cell
        }
        
        
     
    }
    
    //MARK: - category tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView{
            let cell = ProviderProfileEditTableView.dequeueReusableCell(withIdentifier: "SecondCategorycell", for: indexPath) as! SecondCategorycell
            let ss = newcatarray.filter{$0.isselected}.map{$0.name ?? ""}
            
            if ss.count < 3 {
                searchArrRes[indexPath.row].isselected = !searchArrRes[indexPath.row].isselected
                let index = newcatarray.firstIndex(where: { $0.id ==  searchArrRes[indexPath.row].id }) ?? 0
                newcatarray[index].isselected = searchArrRes[indexPath.row].isselected
                selectedcategories = newcatarray.filter{$0.isselected}.map{$0.name ?? ""}
                SelectedCategoryID = newcatarray.filter{$0.isselected}.map{$0.id ?? ""}
                print("ss count ==",ss.count)
                print("selected cat count ==",selectedcategories.count)
            }
            else{
                print("category selection is restricted to max 3")
                print("selected cat count ==",selectedcategories.count)
                print("ss count ==",ss.count)
                dropstatus = false
            }
            categoryTableView.reloadData()
            ProviderProfileEditTableView.reloadData()
        }
        else{
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == ProviderProfileEditTableView{
            if infoClicked == true{
                if indexPath.section == 1{
                    return 0
                }else if indexPath.section == 2{
                    return 0
                }else if indexPath.section == 3{
                    return 0
                }else if indexPath.section == 6{
                    if areaOfServiceClicked == true{
                            return 1000
                    }else{
                        return UITableView.automaticDimension
                    }
                }
            }else{
                if indexPath.section == 2{
                    if dropstatus{
                        return 500
                    }else{
                        return 70
                    }
                }else{
                    return UITableView.automaticDimension
                }
            }
            
            if areaOfServiceClicked == true{
                if indexPath.section == 5{
                    return 0
                }
            }else{
                if indexPath.section == 5{
                    return UITableView.automaticDimension
                }
            }
            
//            if statedropstatusForProviderProviderprofileEdit == true{
//                if indexPath.section == 6{
//                    return 1000
//                }else{
//                    return UITableView.automaticDimension
//                }
//            }
            
            return UITableView.automaticDimension
        }else{
            return UITableView.automaticDimension
        }
    }
    
    
}

extension ProviderMyProfileEditVC: CLLocationManagerDelegate {
    func showResult(string:String){
        var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                    self.locationView.isHidden = false
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                    self.locationTableview.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    func hideResult(){
        self.locationView.isHidden = true
        poplocationTextfield.text = ""
        autocompleteResults.removeAll()
        locationTableview.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        hideResult()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == poplocationTextfield{
            let text = textField.text! as NSString
            let fullText = text.replacingCharacters(in: range, with: string)
            if fullText.count > 0 {
                showResult(string:fullText)
            }else{
                hideResult()
            }
        }
        else if textField == catSearchTF{
            
        }
        else{
            
        }
        
        return true
    }
    
    //show location popup
    @objc func showLocationPopUp(){
        blurView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height:  kScreenHeight))
        blurView.backgroundColor = .black.withAlphaComponent(0.2)
        view.addSubview(blurView)
        let h = autocompleteResults.count * 30
        self.locationPopupView.frame = CGRect(x: (kScreenWidth - 320) / 2, y: (kScreenHeight - 400) / 2, width: 320, height: 400)
        blurView.addSubview(self.locationPopupView)
        blurView.bringSubviewToFront(self.locationPopupView)
        locationPopupView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 0.33, animations: {
            self.locationPopupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    //hide location popup
    func didmissLocationPopUp() {
        if blurView != nil {
            UIView.animate(withDuration: 0.33, animations: {
                self.blurView.alpha = 0
            }, completion: { (completed) in
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            }, completion: { (completed) in
                self.blurView.gestureRecognizers?.removeAll()
                self.blurView.removeFromSuperview()
                self.blurView = nil
            })
        }
    }
    
    
    
    
    
    
    //
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isLocationHasValue{
                    return
                }
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let userLocation:CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if let placemark = placemarks {
                if placemark.count>0 {
                    self.isLocationHasValue = true
                    let placemark = placemarks?.first
                    let loc = placemark?.locality ?? ""
                    let area = placemark?.administrativeArea ?? ""
                    let Country = placemark?.country ?? ""
                    
                    self.getlocality = loc
                    self.getcountry = Country
                    
                    self.getlocality = loc
                    self.getcountry = Country
                    self.getlat = (" \(userLocation.coordinate.latitude)")
                    self.getlong = ("\(userLocation.coordinate.longitude)")
                    self.selectedLocationFromPopup = "\(self.getlocality) , \(self.getcountry)"
                    
                }
            }
            self.ProviderProfileEditTableView.reloadData()
        }
    }
}

extension ProviderMyProfileEditVC{
    
    //MARK: - category api
        func getCategoryAPI() {
         //showActivityIndicator()
          //  let url = "http://13.234.177.61/api7/getCategories"
            let url = kBaseUrl+"getCategories"
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
           
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
                print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                 //hideActivityIndicator()
                                categorylist = getCategoriesResponse(from: responsedata)

                                
                               newcatarray = categorylist?.Data ?? []
                                DispatchQueue.main.async {
                                    /*if let categories = UserDefaults.standard.value(forKey: "selectedIDs") as? [String]{
                                        newcatarray.forEach({ $0.isselected = categories.contains($0.id ?? "") })
                                        selectedcategories = newcatarray.filter({ $0.isselected }).map({ $0.name ?? "" })
                                        SelectedCategoryID = newcatarray.filter({ $0.isselected }).map({ $0.id ?? "" })
                                    }*/
                                    searchArrRes = newcatarray
                                    self.categoryTableView.reloadData()
                                    self.ProviderProfileEditTableView.reloadSections([2], with: .none)
                                }
                               }
                   
               
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
            }
        }
    
    
    func showResults(string:String){
      var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                 
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                   
                    print("loc table count",self.autocompleteResults.count)
                   
                    self.ProviderProfileEditTableView.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    
    
    //MARK: - User details API Call
        func getProviderProfileDetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            
            let params = ["userId": userid] as [String : Any]
            
            let url = kBaseUrl+"getUserDetails"
          //  let url = "http://13.234.177.61/api7/getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            //print("responsedata",responsedata)
                            self.userdetails = getUserDetailsResponse(from:responsedata)
                            
                            //self.TF_Name.text = self.userdetails?.Data?.fullName ?? ""
                            //self.label1.text = self.userdetails?.Data?.categoryIds[0].name ?? ""
                            //self.label2.text = self.userdetails?.Data?.categoryIds[1].name ?? ""
                            //self.label3.text = self.userdetails?.Data?.categoryIds[2].name ?? ""
                            //self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profile icon"))
                        }
                        self.ProviderProfileEditTableView.reloadData()
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
}
