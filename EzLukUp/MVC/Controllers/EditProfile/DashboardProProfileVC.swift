//
//  DashboardProProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/07/23.
//

import UIKit
import SwiftUI
import Kingfisher
import Alamofire
import Contacts

class DashboardProProfileVC: UIViewController, UITextFieldDelegate {

    //MARK: - Outlets
    @IBOutlet weak var collectionBTN: BaseButton!
    @IBOutlet weak var collectionIMG: UIImageView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var connectionLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var catview1: BaseView!
    @IBOutlet weak var catview2: BaseView!
    @IBOutlet weak var catview3: BaseView!
    
    @IBOutlet weak var stackviewCat: UIStackView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var serviceBTNView: BaseView!
    @IBOutlet weak var infoBTNView: BaseView!
    @IBOutlet weak var feedbackBTNView: BaseView!
    @IBOutlet weak var servicesLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var containerView3: UIView!
    @IBOutlet weak var collectionPopupView: UIView!
    @IBOutlet weak var collectionviewBlur: UIView!
    
    var userdetails : getUserDetailsResponse?
    var selectedBtn  = 0 // if 0 selected is servicesbtn , 1- infobtn selected , 2- feedbackbtn selected
    var pickedimage :UIImage?
    var categorycount = 0
    var selecteduserid = "" // selected contactid from contactmainVC
    var getname = ""
    var getmobile = ""
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
   // let contactuserId : String = UserDefaults.standard.value(forKey: "KcontactuserId") as? String ?? ""
    
    var mobnumofprovider = ""
    var checkedbtnstatus : Bool = false
    
    var getid = ""
    var checkfeedback = ""
    var checkconsufeedback = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getuserdetailsapi()
        //checkfeedreportdata()
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getuserdetailsapi()
//        if checkcollectionstatus == "fromsearch"{
//            self.searchpopupview.isHidden = true
//            self.searchblurview.isHidden = true
//            self.collectionIMG.image = UIImage(named: "collection_icon")
//        }else{
//            collectionPopupView.isHidden = true
//            collectionviewBlur.isHidden = true
//            self.collectionIMG.image = UIImage(named: "collection_icon")
//        }
    }
    
    //MARK: - SETUP_FUNCTIONS
    func setupUI(){
        selectedBtn = 0
        stackview.layer.cornerRadius = stackview.frame.height / 2
        infoBTNView.backgroundColor = UIColor.clear
        feedbackBTNView.backgroundColor = UIColor.clear
        servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.containerView1.isHidden = false
        self.containerView2.isHidden = true
        self.containerView3.isHidden = true
        self.collectionPopupView.isHidden = true
        self.collectionviewBlur.isHidden = true
        
    }
    
   
    
    func checkfeedreportdata(){
        print("chakka",checkfeedback)
        if checkconsufeedback == "fromdata"{
            selectedBtn = 2
            serviceBTNView.backgroundColor = UIColor.clear
            infoBTNView.backgroundColor = UIColor.clear
            feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
            servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            self.containerView1.isHidden = true
            self.containerView2.isHidden = true
            self.containerView3.isHidden = false
            self.collectionPopupView.isHidden = true
            self.collectionviewBlur.isHidden = true
            
        }
    }
    
 


    
}

//MARK: - @IBActions & functions
extension DashboardProProfileVC {
    
    
       @IBAction func backBTNAction(_ sender: UIButton) {
           self.nameLbl.text?.removeAll()
           self.navigationController?.popViewController(animated: true)
       }
       
       @IBAction func collectionBlurBTN(_ sender: UIButton) {
           if collectionPopupView.isHidden{
               collectionPopupView.isHidden = false
               collectionviewBlur.isHidden = false
               self.collectionIMG.image = UIImage(named: "selectcollection")
               
           }
           else{
               collectionPopupView.isHidden = true
               collectionviewBlur.isHidden = true
               self.collectionIMG.image = UIImage(named: "collection_icon")
           }
           
   //        if checkcollectionstatus == "fromsearch"{
   //           // if searchpopupview.isHidden{
   //                searchpopupview.isHidden = false
   //                searchblurview.isHidden = false
   //                self.collectionIMG.image = UIImage(named: "selectcollection")
   ////            }
   ////            else{
   ////                searchpopupview.isHidden = true
   ////                searchblurview.isHidden = true
   ////                self.collectionIMG.image = UIImage(named: "collection_icon")
   ////            }
   //        }
   //        else{
   //          //if collectionPopupView.isHidden{
   //                collectionPopupView.isHidden = false
   //            collectionviewBlur.isHidden = false
   //                searchblurview.isHidden = false
   //                self.collectionIMG.image = UIImage(named: "selectcollection")
   ////            }
   ////            else{
   ////               collectionPopupView.isHidden = true
   ////                searchblurview.isHidden = true
   ////                self.collectionIMG.image = UIImage(named: "collection_icon")
   ////            }
   //        }
       }
       
       
       @IBAction func collectionBTNTapped(_ sender: UIButton) {
           
//           if checkcollectionstatus == "fromsearch"{
//
//               searchpopupview.isHidden = false
//           }else{
//               collectionPopupView.isHidden = false
//               collectionviewBlur.isHidden = false
//           }
//
//           if checkcollectionstatus == "fromsearch"{
//              // if searchpopupview.isHidden{
//                   searchpopupview.isHidden = false
//                   searchblurview.isHidden = false
//               self.collectionIMG.image = UIImage(named: "Collections")
//   //            }
//   //            else{
//   //                searchpopupview.isHidden = true
//   //                searchblurview.isHidden = true
//   //                self.collectionIMG.image = UIImage(named: "collection_icon")
//   //            }
//           }
//           else{
             //if collectionPopupView.isHidden{
                   collectionPopupView.isHidden = false
               collectionviewBlur.isHidden = false
                   self.collectionIMG.image = UIImage(named: "selectcollection")
   //            }
   //            else{
   //               collectionPopupView.isHidden = true
   //                searchblurview.isHidden = true
   //                self.collectionIMG.image = UIImage(named: "collection_icon")
   //            }
          // }
       }
       
       @IBAction func serviceBTNTapped(_ sender: UIButton) {
           selectedBtn = 0
           infoBTNView.backgroundColor = UIColor.clear
           feedbackBTNView.backgroundColor = UIColor.clear
           serviceBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
           servicesLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
           infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
           feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
           self.containerView1.isHidden = false
           self.containerView2.isHidden = true
           self.containerView3.isHidden = true
           self.collectionPopupView.isHidden = true
           self.collectionviewBlur.isHidden = true
       }
       
       @IBAction func infoBTNTapped(_ sender: UIButton) {
           selectedBtn = 1
           serviceBTNView.backgroundColor = UIColor.clear
           feedbackBTNView.backgroundColor = UIColor.clear
           infoBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
           servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
           feedbackLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
           infoLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
           self.containerView1.isHidden = true
           self.containerView2.isHidden = false
           self.containerView3.isHidden = true
           self.collectionPopupView.isHidden = true
           self.collectionviewBlur.isHidden = true
           
       }
       
       
       @IBAction func feedbackBTNTapped(_ sender: UIButton) {
           selectedBtn = 2
           serviceBTNView.backgroundColor = UIColor.clear
           infoBTNView.backgroundColor = UIColor.clear
           feedbackBTNView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
           servicesLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
           infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
           feedbackLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
           self.containerView1.isHidden = true
           self.containerView2.isHidden = true
           self.containerView3.isHidden = false
           self.collectionPopupView.isHidden = true
           self.collectionviewBlur.isHidden = true
       }
       
       
       //MARK:- PopupView Actions
       @IBAction func shareBTNTapped(_ sender: UIButton) {
           let contact = createContact(fname: self.nameLbl.text ?? "", lname: "", mob:  self.mobnumofprovider)
           
           do {
               try self.shareContacts(contacts: [contact])
           }
           catch {
               print("failed to share contact with some unknown reasons")
           }
       }
       //MARK: - share contact card from  menu
       func createContact(fname:String,lname:String,mob:String) -> CNContact {
           // Creating a mutable object to add to the contact
           let contact = CNMutableContact()
           contact.imageData = NSData() as Data // The profile picture as a NSData object
           contact.givenName = fname
           contact.familyName = lname
           contact.phoneNumbers = [CNLabeledValue(
               label:CNLabelPhoneNumberiPhone,
               value:CNPhoneNumber(stringValue:mob))]
           
           return contact
       }
      
       func shareContacts(contacts: [CNContact]) throws {
           
           guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
               return
           }
           
           var filename = NSUUID().uuidString
           
           // Create a human friendly file name if sharing a single contact.
           if let contact = contacts.first, contacts.count == 1 {
               
               if let fullname = CNContactFormatter().string(from: contact) {
                   filename = fullname.components(separatedBy: " ").joined(separator: "")
               }
           }
           
           let fileURL = directoryURL
               .appendingPathComponent(filename)
               .appendingPathExtension("vcf")
           
           let data = try CNContactVCardSerialization.data(with: contacts)
           
           print("filename: \(filename)")
           print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
           
           try data.write(to: fileURL, options: [.atomicWrite])
           
           let activityViewController = UIActivityViewController(
               activityItems: [fileURL],
               applicationActivities: nil
           )
           
           present(activityViewController, animated: true, completion: nil)
       }
       @IBAction func feedbackPopupBTNTapped(_ sender: UIButton) {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
           vc.getcontactid = self.providerid
           self.navigationController?.pushViewController(vc, animated: true)
       }
       
       @IBAction func reportBTNTapped(_ sender: UIButton) {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
           vc.getcontactid = self.providerid
           self.navigationController?.pushViewController(vc, animated: true)
       }
       
       @IBAction func editBTNTapped(_ sender: UIButton) {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "EditProviderVC") as! EditProviderVC
           vc.getcontactid = self.selecteduserid
           self.navigationController?.pushViewController(vc, animated: true)
       }
     
   
 
}


extension DashboardProProfileVC{
   func getuserdetailsapi(){
       
       let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
       let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
       
       let params = ["userId": providerid] as [String : Any]
       
       let url = kBaseUrl+"getUserDetails"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
               switch (response.result) {
               case .success( let JSON):
                   
                   if let responsedata =  JSON as? [String:Any]  {
                       //print("responsedata",responsedata)
                       self.userdetails = getUserDetailsResponse(from:responsedata)
                       
                       self.nameLbl.text = self.userdetails?.Data?.fullName ?? ""
                       
                       //Setup Companyname
                                         // print(self.userdetails?.Data?.companyname)
                       if self.userdetails?.Data?.companyName == ""{
                                              self.companyName.text = ""
                                          }else{
                                              self.companyName.text = "@ \(self.userdetails?.Data?.companyName ?? "")"
                                          }
                       
                       self.label1.text = self.userdetails?.Data?.categoryIds[0].name ?? ""
//                            self.label2.text = self.userdetails?.Data?.categoryIds[1].name ?? ""
//                            self.label3.text = self.userdetails?.Data?.categoryIds[2].name ?? ""
                       self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Data?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
                   }
                   
               case .failure(let error):
                   print("Request error: \(error.localizedDescription)")
               }
           
           
       }
   }
}
