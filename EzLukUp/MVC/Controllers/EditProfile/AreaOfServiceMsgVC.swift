//
//  AreaOfServiceMsgVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 31/07/23.
//

import UIKit

var checkareastatus : Bool = false

class AreaOfServiceMsgVC: UIViewController {

    @IBOutlet weak var checkIMG: UIImageView!
    @IBOutlet weak var closeBTN: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkIMG.image = UIImage(named: "unselectcheckbox")
        closeBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
       
    }
    
    @IBAction func checkBTNTapped(_ sender: UIButton) {
        if checkareastatus == false {
            checkareastatus = true
            self.checkIMG.image = UIImage(named: "selectcheckbox")
        }else{
            checkareastatus = false
            self.checkIMG.image = UIImage(named: "unselectcheckbox")
        }
    }
    
    @IBAction func dismissBTNTapped(_ sender: UIButton) {
        if checkclose == "closevc"{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        if checkclose == "closevc"{
            self.navigationController?.popViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}
