//
//  DashProvProfileFeedbackVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 02/07/23.
//

import UIKit
import Alamofire
import Kingfisher

class DashProvProfileFeedbackVC: UIViewController {

    @IBOutlet weak var feedbacktableview: UITableView!
    @IBOutlet weak var nodataLBL: BaseView!
    
    var userdetails : getProviderUserDetailsResponse?
    var ktype = ""
    var getfeedbackid = ""
   // let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
     getuserdetails()
     
    }
   
    func convertisoformat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: dateString)!
        let timeInterval = date.timeIntervalSinceNow
        
        let secondsAgo = Int(abs(timeInterval))
        let minutesAgo = secondsAgo / 60
        let hoursAgo = minutesAgo / 60
        let daysAgo = hoursAgo / 24
        let weeksAgo = daysAgo / 7
        let monthsAgo = weeksAgo / 4
        let yearsAgo = monthsAgo / 12
        
        if secondsAgo < 60 {
            return "\(secondsAgo) seconds ago"
        } else if minutesAgo < 60 {
            return "\(minutesAgo) minutes ago"
        } else if hoursAgo < 24 {
            return "\(hoursAgo) hours ago"
        } else if daysAgo < 7 {
            return "\(daysAgo) days ago"
        } else if weeksAgo < 4 {
            return "\(weeksAgo) weeks ago"
        } else if monthsAgo < 12 {
            return "\(monthsAgo) months ago"
        } else {
            return "\(yearsAgo) years ago"
        }
    }
   

}

//MARK: - Feedback UITableViewDelegate,UITableViewDataSource
extension DashProvProfileFeedbackVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userdetails?.Feedback.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = feedbacktableview.dequeueReusableCell(withIdentifier: "DashProFeedbackTVCell", for: indexPath) as! DashProFeedbackTVCell
        cell.feedbackLbl.text = self.userdetails?.Feedback[indexPath.row].feedback ?? ""
        cell.nameLbl.text = self.userdetails?.Feedback[indexPath.row].userId?.fullName ?? "#unabletofetchName"
        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.userdetails?.Feedback[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
        let createdDateString = self.userdetails?.Feedback[indexPath.row].createdAt ?? ""
        let daysagoString = convertisoformat(dateString: createdDateString)
        print(daysagoString)
        cell.dayLbl.text = daysagoString
        
        
        ktype = self.userdetails?.Feedback[indexPath.row].type ?? ""
        if ktype == "feedback"{
                cell.feedbackview.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
            }else{
                cell.feedbackview.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


//MARK: - TableViewCell
class DashProFeedbackTVCell: UITableViewCell {
    
    @IBOutlet weak var feedbackview: BaseView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    
}


extension DashProvProfileFeedbackVC{
    //MARK: - User details API Call
        func getuserdetails(){
              
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
            let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
            let providerid : String = UserDefaults.standard.value(forKey: "KproviderId") as! String
            
            let params = ["userId": "providerid"] as [String : Any]
            
            let url = kBaseUrl+"getUserDetails"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                    print(response)
                    switch (response.result) {
                    case .success( let JSON):
                        
                        if let responsedata =  JSON as? [String:Any]  {
                            //print("responsedata",responsedata)
                            self.userdetails = getProviderUserDetailsResponse(from:responsedata)
//                            if self.userdetails?.Feedback.count == 0{
//                                self.nodataLBL.isHidden = false
//                            }else{
//                                self.nodataLBL.isHidden = true
//                            }
                            
                        }
                        DispatchQueue.main.async {
                            self.feedbacktableview.reloadData()
                        }
                        
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                    }
                
                
            }
        }
}
