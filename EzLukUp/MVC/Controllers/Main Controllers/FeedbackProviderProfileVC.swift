//
//  FeedbackProviderProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import Alamofire
import Kingfisher

var getfeedbackdetails : getProviderProfileResponse?

class FeedbackProviderProfileVC: UIViewController {
    
    @IBOutlet weak var feedbacktableview: UITableView!
    @IBOutlet weak var nodataLBL: UIView!
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    var isoformatmodel = ""
    var ktype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nodataLBL.isHidden = true
   //    feedbackapi()
        setupdetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateinfo(notification:)), name: Notification.Name("info"), object: nil)
    }
    
    @objc func navigateinfo(notification : Notification){
       // getinfolist == getinfolist
        feedbacktableview.reloadData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       // feedbackapi()
    }
    
    func setupdetails(){
        if getfeedbackdetails?.Feedback.count == 0{
            self.nodataLBL.isHidden = false
        }else{
            self.nodataLBL.isHidden = true
        }
    }
}


//MARK: - Feedback UITableViewDelegate,UITableViewDataSource
extension FeedbackProviderProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getfeedbackdetails?.Feedback.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = feedbacktableview.dequeueReusableCell(withIdentifier: "FeedbackTVCell", for: indexPath) as! FeedbackTVCell
        cell.feedbackLbl.text = getfeedbackdetails?.Feedback[indexPath.row].feedback
       
       // cell.nameLbl.text = getfeedbackdetails?.Feedback[indexPath.row].userId?.fullName
        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (getfeedbackdetails?.Feedback[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
        
        self.isoformatmodel = getfeedbackdetails?.Feedback[indexPath.row].createdAt ?? ""
        let daysagoString = convertisoformat(dateString: isoformatmodel)
          print(daysagoString)
          cell.dayLbl.text = daysagoString
        
    
//MARK:- Feedback/Report border colouring
        
        ktype = getfeedbackdetails?.Feedback[indexPath.row].type ?? ""
        if ktype == "feedback"{
                cell.feedbackview.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
            }else{
                cell.feedbackview.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
            }

        
        
//MARK:- Level of connections adding
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
        var name : String = getfeedbackdetails?.Feedback[indexPath.row].userId?.fullName as! String
        
        if getfeedbackdetails!.Feedback[indexPath.row].userId != nil && getfeedbackdetails!.Feedback[indexPath.row].userId!.firstLevelConnections!.contains(userid){
            cell.nameLbl.text = name + " .1st"
                }else if getfeedbackdetails!.Feedback[indexPath.row].userId != nil && getfeedbackdetails!.Feedback[indexPath.row].userId!.secondLevelConnections!.contains(userid){
                    cell.nameLbl.text = name + " .2nd"
                       }
                else if getfeedbackdetails!.Feedback[indexPath.row].userId != nil && getfeedbackdetails!.Feedback[indexPath.row].userId!.thirdLevelConnections!.contains(userid){
                    cell.nameLbl.text = name + " .3rd"
                        }
            else{
                cell.nameLbl.text = name
            }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    func convertisoformat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: isoformatmodel)!
        let timeInterval = date.timeIntervalSinceNow
        
        let secondsAgo = Int(abs(timeInterval))
        let minutesAgo = secondsAgo / 60
        let hoursAgo = minutesAgo / 60
        let daysAgo = hoursAgo / 24
        let weeksAgo = daysAgo / 7
        let monthsAgo = weeksAgo / 4
        let yearsAgo = monthsAgo / 12
        
        if secondsAgo < 60 {
            return "\(secondsAgo) seconds ago"
        } else if minutesAgo < 60 {
            return "\(minutesAgo) minutes ago"
        } else if hoursAgo < 24 {
            return "\(hoursAgo) hours ago"
        } else if daysAgo < 7 {
            return "\(daysAgo) days ago"
        } else if weeksAgo < 4 {
            return "\(weeksAgo) weeks ago"
        } else if monthsAgo < 12 {
            return "\(monthsAgo) months ago"
        } else {
            return "\(yearsAgo) years ago"
        }
    }
 }


extension FeedbackProviderProfileVC{
//MARK: - API Call
func feedbackapi(){
//    let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
    
    let params = ["contactid": contactid] as [String : Any]
    
    let url = kBaseUrl+"getProviderContactProfileDetails"
   // let url = "http://13.234.177.61/api7/getProviderContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    getfeedbackdetails = getProviderProfileResponse(from:responsedata)
                    if getfeedbackdetails?.Feedback.count == 0{
                        self.nodataLBL.isHidden = false
                    }else{
                        self.nodataLBL.isHidden = true
                    }
                    
                    
                    self.feedbacktableview.reloadData()
                   // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}


//MARK: - TableViewCell
class FeedbackTVCell: UITableViewCell {
    
    @IBOutlet weak var feedbackview: BaseView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    
    
}
