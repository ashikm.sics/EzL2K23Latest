//
//  TagProviderScrollVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 16/07/23.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI


class TagProviderScrollVC: UIViewController {

    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameTF: UILabel!
    @IBOutlet weak var searchIMG: UIImageView!
    @IBOutlet weak var closeCategoryBTN: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryTF: UITextField!
    @IBOutlet weak var categorytableview: UITableView!
    @IBOutlet weak var aboutTxtView: UITextView!
    @IBOutlet weak var recomIMG: UIImageView!
    @IBOutlet weak var recomBTN: UIButton!
    @IBOutlet weak var recomLabel: UILabel!
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var categoryDropView: BaseView!
    @IBOutlet weak var aboutview: BaseView!
    
    
    var tagproviderlist : tagProviderResponse?
    var TagsearchArrRes = [getcatDataModel]()
    var Tagnewcatarray = [getcatDataModel]()
    var Tagcategorylist : getCategoriesResponse?
    var TagselectedcategoryID = [String]()
    var tagcatID = String()
    var Tagselectedcategory = [String]()
    var tagsearchCat = String()
    var checkedbtnstatus : Bool = false
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    var getcontactid = String()
    var getProfileName = String()
    var getProfileIMG = String()
    var phNo = ""
    var countrycode = ""
    var fromwhichlist = ""
    var typedabout = ""
    var itemz = String() // cat name from listpage
    var tagCatID = String()// cat id from listpage
    var generalData : GeneralListData?
    var tagCompletion : (() -> ())? = nil
    func tagCompletionCallBack(completion : @escaping () -> ()){
        self.tagCompletion = completion
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.nameTF.text = getProfileName
        self.profileIMG.kf.setImage(with: URL(string: kImageUrl + (getProfileIMG)),placeholder: UIImage(named: "profileimgIfnodata"))
        self.aboutview.isHidden = true
        if fromwhichlist == "tagged"{
            self.Tagged_getcategoryapi(contactphonNum: phNo)
//            getRecomendedCount(catid:TagselectedcategoryID)
            self.categoryLabel.isHidden = false
            self.categoryLabel.text = itemz
            
          //  self.categoryLabel.text = ""
            //self.categoryTF.text = ""
            self.closeCategoryBTN.isHidden = true
            self.closeCategoryBTN.isUserInteractionEnabled = false
            self.categoryTF.isHidden = true
            self.searchIMG.isHidden = true
            if itemz.isEmpty{
                Tagselectedcategory.removeAll()
                TagselectedcategoryID.removeAll()
            }else{
                Tagselectedcategory.removeAll()
                TagselectedcategoryID.removeAll()
                Tagselectedcategory.append(itemz)
                self.categoryLabel.text = itemz
               // TagselectedcategoryID.append(tagCatID)
                
            }
            TagselectedcategoryID.removeAll()
            tagcatID = tagCatID
            TagselectedcategoryID.append(tagcatID)
           // tagsearchCat = self.TagsearchArrRes[inde
            getRecomendedCount(catid:TagselectedcategoryID)
        }
        else if fromwhichlist == "possible"{
            self.getcategoryapi()
            self.categoryLabel.isHidden = true
            self.categoryLabel.text = ""
            self.categoryTF.text = ""
            self.closeCategoryBTN.isHidden = true
            self.categoryTF.isHidden = false
            self.searchIMG.isHidden = false
            //Tagselectedcategory.removeAll()
            TagselectedcategoryID.removeAll()
            tagcatID = tagCatID
            TagselectedcategoryID.append(tagcatID)
           // tagsearchCat = self.TagsearchArrRes[inde
            getRecomendedCount(catid:TagselectedcategoryID)
        }
        else if fromwhichlist == "normal"{
            self.getcategoryapi()
            self.categoryLabel.isHidden = true
            self.categoryLabel.text = ""
            self.categoryTF.text = ""
            self.closeCategoryBTN.isHidden = true
            self.categoryTF.isHidden = false
            self.searchIMG.isHidden = false
            //Tagselectedcategory.removeAll()
            TagselectedcategoryID.removeAll()
            tagcatID = tagCatID
            TagselectedcategoryID.append(tagcatID)
           // tagsearchCat = self.TagsearchArrRes[inde
            getRecomendedCount(catid:TagselectedcategoryID)
        }
        else{
            
        }
        
        self.recomIMG.image = UIImage(named: "unselectcheckbox")
        
       
    }
   
    func setupUI(){
        aboutTxtView.delegate = self
        aboutTxtView.text = "Enter your feedback for the services"
        aboutTxtView.textColor = UIColor.black
        aboutTxtView.returnKeyType = .done
    }

    @IBAction func backBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dismissBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    @IBAction func closeCategoryBTNTapped(_ sender: UIButton) {
        self.categoryLabel.isHidden = true
        self.categoryLabel.text = ""
        self.categoryTF.text = ""
        self.closeCategoryBTN.isHidden = true
        self.categoryTF.isHidden = false
        self.searchIMG.isHidden = false
    }
    
    
    @IBAction func recomBTNTapped(_ sender: UIButton) {
        if checkedbtnstatus == false {
            checkedbtnstatus = true
            self.recomIMG.image = UIImage(named: "selectcheckbox")
            self.aboutview.isHidden = false
        }else{
            checkedbtnstatus = false
            self.recomIMG.image = UIImage(named: "unselectcheckbox")
            self.aboutview.isHidden = true
        }
        }
    
    @IBAction func saveBTNTapped(_ sender: UIButton) {
        
        if categoryLabel.text == ""{
            showDefaultAlert(viewController: self, title: "Message", msg: "Please select a category")
        } else{
            tagproviderapi()
        }
       
    }

}



//MARK: - TextfieldDelegates & TextviewDelegates
extension TagProviderScrollVC: UITextFieldDelegate,UITextViewDelegate{
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if aboutTxtView.text == "Enter your feedback for the services" {
                    aboutTxtView.text = ""
                    aboutTxtView.textColor = UIColor.black
                    aboutTxtView.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if aboutTxtView.text == "" {
                    aboutTxtView.text = "Enter your feedback for the services"
                    aboutTxtView.textColor = UIColor.black
                    aboutTxtView.font = UIFont(name: "jost", size: 15.0)
                }else{
                    typedabout = aboutTxtView.text
                }
                
            }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == categoryTF {
            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.categoryDropView.isHidden = newText == ""

            self.TagsearchArrRes = newText == "" ? self.Tagnewcatarray : self.Tagnewcatarray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.categorytableview.reloadData()

        }
        return true
    }
}


//MARK: - TableviewDelegates
extension TagProviderScrollVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.TagsearchArrRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TagcategoryTVCell.identifier) as! TagcategoryTVCell
        cell.categoryLbl.text = self.TagsearchArrRes[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        TagselectedcategoryID.removeAll()
        self.categoryLabel.isHidden = false
        self.closeCategoryBTN.isHidden = false
        self.categoryTF.isHidden = true
        self.searchIMG.isHidden = true
        self.categoryDropView.isHidden = true
//        getRecomendedCount(catid:TagselectedcategoryID)
        print("bllaa",Tagselectedcategory)
        self.categoryLabel.text = self.TagsearchArrRes[indexPath.row].name
        
        tagcatID = self.TagsearchArrRes[indexPath.row].id ?? ""
        TagselectedcategoryID.append(tagcatID)
        getRecomendedCount(catid:TagselectedcategoryID)
       // tagsearchCat = self.TagsearchArrRes[indexPath.row].name ?? ""
        //Tagselectedcategory.append(tagsearchCat)
        
    }
}



extension TagProviderScrollVC{
    //MARK: - category api
        func getcategoryapi() {
            
            let url = kBaseUrl+"getCategories"
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                self.Tagcategorylist = getCategoriesResponse(from: responsedata)
                                self.Tagnewcatarray = self.Tagcategorylist?.Data ?? []
                                DispatchQueue.main.async {
                                    self.TagsearchArrRes = self.Tagnewcatarray
                                    self.categorytableview.reloadData()
                                }
                            }

                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
                 }
          }
    
    
    //MARK: - tag provider API Call
        func tagproviderapi(){
            
        let params: [String : Any] = [
                      "id": getcontactid,
                      "isrecommended": checkedbtnstatus ,
                      "feedback": typedabout,
                      "categoryIds": TagselectedcategoryID
                    ]
        print(params)
            let url = kBaseUrl+"tagContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             switch (response.result) {
                case .success( let JSON):
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata :",responsedata)
                        //  hideActivityIndicator()
                          let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                          alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                              if responsedata["data"] as? String ?? "" == "Updated successfully"{
                                  if let contact = responsedata["contactDetails"] as? [String:Any]{
                                      
                                      DatabaseProviderCategoryEntity().insertfromGeneral(data: contact, catid: self.TagselectedcategoryID.first ?? "", catname: self.categoryLabel.text ?? "")
                                      DatabaseGeneralContactEntity().deletegeneralListData(self.generalData!)
                                      
                                      
                                  }
                                  
  //                                NotificationCenter.default.post(name: Notification.Name("callgeneralapi"), object: nil)
                                  // calls general api for syncing coredata with server
  //                                NotificationCenter.default.post(name: Notification.Name("callproviderapi"), object: nil)
                                 selectedBTN = 1
                                  self.tagCompletion?()
      //                                self.navigationController?.pushViewController(vc, animated: true)
                                  self.navigationController?.popViewController(animated: true)
                                 // self.present(alert, animated: true)
                              }else{
                                  let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                                  
                              }
                              
                              }
                     
                                                        ))
                          self.present(alert, animated: true)
                      }
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                }
         }
    }
    

    
    //category api for tagged contacts
    func Tagged_getcategoryapi(contactphonNum:String) {
    // showActivityIndicator()

        let params = [
            "contactFullPhoneNumber":contactphonNum] as? [String : Any]
 
        let url = kBaseUrl+"availableCategoriesForProvider"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
               
            print(response)
            switch (response.result) {
                       case .success( let JSON):
                        if let responsedata =  JSON as? [String:Any]  {
                            self.Tagcategorylist = getCategoriesResponse(from: responsedata)
                            self.Tagnewcatarray = self.Tagcategorylist?.Data ?? []
                            DispatchQueue.main.async {
                                self.TagsearchArrRes = self.Tagnewcatarray
                                self.categorytableview.reloadData()
                            }
                        }
               
                        case .failure(let error):
                           print("Request error: \(error.localizedDescription)")
                    }
             }
      }
    

    //MARK: - Api for enabling and disabling check box (max recomendation count 2)
//    getRecommendCount
    
    func getRecomendedCount(catid:[String]) {
  //   showActivityIndicator()
        let params = [
            "userid": userid,
                "categoryids": catid] as? [String : Any]
        print(params)
        let url = kBaseUrl+"getRecommendCount"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            
            print(response)
            switch (response.result) {
                       case .success( let JSON):
                        if let responsedata =  JSON as? [String:Any]  {
                          //  hideActivityIndicator()
                            if responsedata["recomendCount"] as? Int ?? 0 >= 2{
                                print("checkbox is hidden")
                                self.recomBTN.isUserInteractionEnabled = false
                                self.recomBTN.layer.borderColor = #colorLiteral(red: 0.6677497029, green: 0.6677497029, blue: 0.6677497029, alpha: 1)
                                self.recomBTN.layer.borderWidth = 2
                                self.recomLabel.textColor = #colorLiteral(red: 0.6677497029, green: 0.6677497029, blue: 0.6677497029, alpha: 1)
                                self.recomIMG.isHidden = true
                                self.noteLbl.text = "Note - You have already recommended 2 Providers for this Category, hence cannot recommend more. If you want to recommend this provider then first remove recommendation for one of the other ones."
                            }
                            else{
                                print("checkbox show ")
                                self.recomBTN.isUserInteractionEnabled = true
                               // self.recomBTN.layer.borderColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
                                self.recomLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                self.recomBTN.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                self.recomBTN.layer.borderWidth = 0
                                self.recomIMG.isHidden = false
                                self.recomIMG.image = UIImage(named: "unselectcheckbox")
                                self.noteLbl.text = "Note - Any number of Providers can be tagged to a Category, but maximum 2 can be Recommended for a Category."
                            }
                           }
               
                        case .failure(let error):
                           print("Request error: \(error.localizedDescription)")
                    }
             }
      }

}

class TagcategoryTVCell: UITableViewCell{
    static let identifier = String(describing: TagcategoryTVCell.self)

    @IBOutlet weak var categoryLbl: UILabel!
}
