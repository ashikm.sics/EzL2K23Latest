//
//  SubProfileProviderVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/11/22.
//

import UIKit
import Alamofire
import Kingfisher

var providerlistS : getGeneralProfileResponse?

class SubProfileProviderVC: UIViewController {
    
    @IBOutlet weak var providerTableview: UITableView!
    @IBOutlet weak var nodataLBL: UILabel!
    
 //   var providerlistS : getGeneralProfileResponse?
    var selectid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nodataLBL.isHidden = true
        DispatchQueue.main.async {
          //  self.api()
            print("providercount",providerlistS?.RecommendedProviders.count ?? 0)
            if providerlistS?.RecommendedProviders.count == 0 {
                self.nodataLBL.isHidden = false
            }else{
                self.nodataLBL.isHidden = true
            }
        }
      
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateinfo(notification:)), name: Notification.Name("generaldetails"), object: nil)
    }
//    override func viewWillAppear(_ animated: Bool) {
//        self.api()
//    }
    @objc func navigateinfo(notification : Notification){
        providerTableview.reloadData()
    }
}

//MARK: - @IBActions
extension SubProfileProviderVC{
    
    @IBAction func dropdownBTNTapped(_ sender: UIButton) {
        
        providerlistS?.RecommendedProviders[sender.tag].sectionOpened = !(providerlistS?.RecommendedProviders[sender.tag].sectionOpened ?? true)
        providerTableview.reloadData()
    }
    
    @IBAction func dotsBTNTapped(_ sender: UIButton) {
        print("indexpath :",sender.tag)
    }
}


//MARK: - UITableViewDelegate,UITableViewDataSource
@available(iOS 14.0, *)
extension SubProfileProviderVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return providerlistS?.RecommendedProviders.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return providerlistS?.RecommendedProviders[section].contacts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = providerTableview.dequeueReusableCell(withIdentifier: "Headercell") as! Headercell
        cell.headerNameLbl.text = providerlistS?.RecommendedProviders[section].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = providerTableview.dequeueReusableCell(withIdentifier: "ProviderContactsCell", for: indexPath) as! ProviderContactsCell
        cell.providerlistS = providerlistS
        cell.nameLbl.text = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].name ?? ""
        
 //Location:-
        let city = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].areaOfService.first?.city ?? ""
        let stateshortcode = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].areaOfService.first?.stateShortCode ?? ""
        let country = "USA"
        if city != "" && stateshortcode != ""{
            cell.locationLbl.text = "\(city) , \(stateshortcode)"
        }else{
            if city == ""{
                cell.locationLbl.text = stateshortcode
            }else{
                cell.locationLbl.text = city
            }
        }
        
        
//Recommendations
        let rec = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].recomended.first?.userId.count
        headcount = rec ?? 0
        print("headcount",rec)
        //
        cell.reccomendCVWidth.constant = CGFloat(20 * headcount)
        if headcount > 0 {
            cell.countLabel.text = "\(headcount)"
            cell.reccomndCV.reloadData()
        }
        else {
            cell.countLabel.text = ""
        }
        
        
        cell.providerDotsBTN.tag = indexPath.row
        cell.providerDotsBTN.showsMenuAsPrimaryAction = true
        
            let Invite = UIAction(title:"Invite",
                                image: UIImage(named: "invite")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in

                let Mobnumber = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].phoneNumber ?? ""
                let CCode = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].countryCode ?? ""
                let CId = providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.row].id ?? ""
                
                self.inviteapi(number: CCode.appending(Mobnumber))
                
                      print("invite")
                  }


                  let Share = UIAction(title: "Share",
                                       image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                     // Perform action
                      // text to share
                              let text = "This is some text that I want to share."

                              // set up activity view controller
                              let textToShare = [ text ]
                              let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                              activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

                              // exclude some activity types from the list (optional)
                              activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                              // present the view controller
                              self.present(activityViewController, animated: true, completion: nil)
                      print("sharing")
                   }

//            let Feedback = UIAction(title: "Feedback",
//                                image: UIImage(named: "Feedback")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
//                                 ) { action in
//                    // Perform action
//                    print("typing")
//             }
//
//            let Report = UIAction(title: "Report",
//                                image: UIImage(named: "Report")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
//                                 ) { action in
//                    // Perform action
//                    print("reporting")
//             }

            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
                                 ) { action in
                    // Perform action
                    print("opening")
             }

            cell.providerDotsBTN.menu = UIMenu(title:"", children: [Invite, Edit, Share])
       
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = 0.0
        } else {
          
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return UITableView.automaticDimension
        return providerlistS?.RecommendedProviders.count != 0 && (providerlistS?.RecommendedProviders[indexPath.section].sectionOpened)! ? UITableView.automaticDimension : 0
    }
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =  providerTableview.dequeueReusableCell(withIdentifier: "ProviderContactsCell", for: indexPath) as! ProviderContactsCell
        cell.providerView.isExclusiveTouch = true
        print("hfgcg")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
      //  self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - API call
extension SubProfileProviderVC{
    func api(){
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
         
        let params = ["contactid": contactid] as [String : Any]

     //   let url = "http://13.234.177.61/api7/getGeneralContactProfileDetails"
        let url = kBaseUrl+"getGeneralContactProfileDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            print("params: \(params)")
            print(response)
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    providerlistS = getGeneralProfileResponse(from:responsedata)
                    print("providercount",providerlistS?.RecommendedProviders.count ?? 0)
                    if providerlistS?.RecommendedProviders.count == 0 {
                        self.nodataLBL.isHidden = false
                    }else{
                        self.nodataLBL.isHidden = true
                    }
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            DispatchQueue.main.async {
                self.providerTableview.reloadData()
            }
            
    }
        
    }
    
//MARK: - Invite
    func inviteapi(number : String){
          
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
        
        let params: [String : Any] = ["id": contactid]
        print("params",params)
        let url = kBaseUrl+"inviteContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                      let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                      alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                          if responsedata["data"] as? String ?? "" == "Updated successfully"{
                              self.invitetoShow(number: number)
                              
                          }else{
                              let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                          }
                          
                          }
                                                    ))
                              self.present(alert, animated: true)
                  }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            }
        }
    
    
    func invitetoShow(number:String){
        print(number)
        print("invite normal list")
        let sms = "sms:"+number+"&body="+invitelink
       // let sms = "sms:+919567155224&body="+invitelink
        
        let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        UIApplication.shared.open(URL(string: strURL)!, options: [:], completionHandler: nil)
        
     }

}



//MARK: - Provider Profile contacts cell
class ProviderContactsCell: UITableViewCell {
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dotsIMG: UIImageView!
    @IBOutlet weak var providerDotsBTN: UIButton!
    @IBOutlet weak var providerView: BaseView!
    @IBOutlet weak var locationLbl: UILabel!
    
    @IBOutlet weak var reccomndCV: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var reccomendCVWidth: NSLayoutConstraint!
    var providerlistS : getGeneralProfileResponse?
//    var Mobnumber = ""
//    var CCode = ""
//    var CId = ""
    
//    func inviteapi(number : String){
//  //  func inviteapi(){
//
//        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
//      //  let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
//
//
//        let params: [String : Any] = ["id": CId]
//        print("params",params)
//        let url = kBaseUrl+"inviteContact"
//        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
//            //             print("params:\(params)")
//            //             print("response:\(response)")
//            switch (response.result) {
//            case .success( let JSON):
//                if let responsedata =  JSON as? [String:Any]  {
//
//                    print("responsedata :",responsedata)
//                    if responsedata["data"] as? String == "Updated sucessfully"{
//
//
//
//                     //   print(number)
//                        self.invitetoShow(number: number)
//                    }
//                    else{
//                      //  hideActivityIndicator()
//
//                    }
//                }
//            case .failure(let error):
//                print("Request error: \(error.localizedDescription)")
//            }
//            }
//        }
//
//
//    func invitetoShow(number:String){
//        print(number)
//        print("invite normal list")
//        let sms = "sms:"+number+"&body="+invitelink
//       // let sms = "sms:+919567155224&body="+invitelink
//
//        let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
//        UIApplication.shared.open(URL(string: strURL)!, options: [:], completionHandler: nil)
//
//     }

}


class Headercell: UITableViewCell {
    @IBOutlet weak var dropdownIMG: UIImageView!
    @IBOutlet weak var headerNameLbl: UILabel!
}
class RecomendedCollectionCell: UICollectionViewCell{
    @IBOutlet weak var viewIMGBg: BaseView!
    @IBOutlet weak var recomIMG: UIImageView!
    
}
// MARK: -  recommended collectionview
extension ProviderContactsCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendedCollectionCell", for: indexPath) as! RecomendedCollectionCell
        DispatchQueue.main.async {
            cell.recomIMG.kf.setImage(with: URL(string: kImageUrl + (self.providerlistS?.RecommendedProviders[indexPath.section].contacts[indexPath.item].recomended.first?.userId.first?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = 18
        let h:CGFloat = 18
        return CGSize(width: w, height: h)
    }
}
