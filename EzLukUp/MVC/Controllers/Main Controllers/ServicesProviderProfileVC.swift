//
//  ServicesProviderProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import Alamofire
import SwiftUI
import Kingfisher

var getservivedetails : getProviderProfileResponse?

class ServicesProviderProfileVC: UIViewController {

    @IBOutlet weak var servicestableview: UITableView!
    @IBOutlet weak var noserviceview: UIView!
    
    
    //Outlets
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    var aboutbusiness = ""
    var servicesArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     //  servicedetailsapi()
        setupdetails()
        self.noserviceview.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateinfo(notification:)), name: Notification.Name("info"), object: nil)
    }

    @objc func navigateinfo(notification : Notification){
       // getinfolist == getinfolist
        servicestableview.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // servicedetailsapi()
    }
    
    func setupdetails(){
        self.aboutbusiness = getservivedetails?.Data?.contactUserId?.aboutBusiness ?? ""
      //  self.servicesArray = getservivedetails?.Data?.contactUserId?.serviceDetails as! [String]
        print("bla")
        print(getservivedetails?.Data?.contactUserId?.aboutBusiness)
       
        if getservivedetails?.Data?.contactUserId?.aboutBusiness == nil || getservivedetails?.Data?.contactUserId?.aboutBusiness == "" {
            self.noserviceview.isHidden = false
        }else{
            self.noserviceview.isHidden = true
        }
    }
}


//MARK: - UITableViewDelegate, UITableViewDataSource
extension ServicesProviderProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return servicesArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "ServicesTableCell", for: indexPath) as! ServicesTableCell
            cell.cellLbl1.text = aboutbusiness
            return cell
        }else{
            let cell = servicestableview.dequeueReusableCell(withIdentifier: "SecondServiceTVCell", for: indexPath) as! SecondServiceTVCell
            cell.servicecell.text = servicesArray[indexPath.row]
            return cell
        }
        
//        cell.cellLbl2.text = getservivedetails?.Data?.contactUserId?.serviceDetails[indexPath.row].title
//        cell.cellIMG.kf.setImage(with: URL(string: kImageUrl + (getservivedetails?.Data?.contactUserId?.serviceDetails[indexPath.row].url ?? "")))
       
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension ServicesProviderProfileVC{
//MARK: - API Call
func servicedetailsapi(){
   
    let params = ["contactid": contactid] as [String : Any]
    
    let url = kBaseUrl+"getProviderContactProfileDetails"
   // let url = "http://13.234.177.61/api7/getProviderContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    getservivedetails = getProviderProfileResponse(from:responsedata)
//                    var count = getservivedetails?.Data
                    self.aboutbusiness = getservivedetails?.Data?.contactUserId?.aboutBusiness ?? ""
                    self.servicesArray = getservivedetails?.Data?.contactUserId?.serviceDetails as! [String]
                    print("bla")
                    print(getservivedetails?.Data?.contactUserId?.aboutBusiness)
                   
                    if getservivedetails?.Data?.contactUserId?.aboutBusiness == nil || getservivedetails?.Data?.contactUserId?.aboutBusiness == "" {
                        self.noserviceview.isHidden = false
                    }else{
                        self.noserviceview.isHidden = true
                    }
                    
                    self.servicestableview.reloadData()
                   
                }
                
                
                
//                DispatchQueue.main.async {
//                    self.servicestableview.reloadData()
//                    if self.servicesArray.count == 0{
//                        self.noserviceview.isHidden = false
//                    }else{
//                        self.noserviceview.isHidden = true
//                    }
//                }
                 
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
     }

 }



//MARK: - TableCell First
class ServicesTableCell: UITableViewCell {
    @IBOutlet weak var cellLbl1: UILabel!
}

//MARK: - TableCell Second
class SecondServiceTVCell: UITableViewCell{
    @IBOutlet weak var servicecell: UILabel!
    
}
