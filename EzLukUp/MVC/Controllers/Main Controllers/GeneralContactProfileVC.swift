//
//  GeneralContactProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 22/11/22.
//

import UIKit
import Alamofire
import Kingfisher
import IPImage
import Contacts
import FirebaseDynamicLinks

//var selecteduserid = ""

class GeneralContactProfileVC: UIViewController,UITextFieldDelegate {

//MARK:- OUTLETS
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var btnStackview: UIStackView!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var providerBtnView: BaseView!
    @IBOutlet weak var infoBtnView: BaseView!
    @IBOutlet weak var providerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var proContainerview: UIView!
    @IBOutlet weak var infoContainerview: UIView!
    @IBOutlet weak var collectionPopupView: UIView!
    @IBOutlet weak var collectionIMG: UIImageView!
    @IBOutlet weak var Blurview: UIView!

    
    @IBOutlet weak var connectionLbl: UILabel!
    
    var getuserdetails : getGeneralProfileResponse?
    var selectedBtn  = 0 // if 0 selected is generalbtn , 1- providerbtn selected
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var pickedimage :UIImage?
    var selecteduserid = "" // selected contactid from contactmainVC
    var categorycount = 0
    var getname = ""
    var getmobile = ""
    var getcountry = ""
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as? String ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        userdetailsapi()
        self.Blurview.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  userdetailsapi()
        collectionPopupView.isHidden = true
        self.Blurview.isHidden = true
        self.collectionIMG.image = UIImage(named: "collection_icon")
    }
    

    
 //MARK: - SETUP FUNCTIONS
        func setupUI(){
           // self.profileImageView.image = UIImage(named: "avatar")
            selectedBtn = 0
            self.Blurview.isHidden = true
            btnStackview.layer.cornerRadius = btnStackview.frame.height / 2
            infoBtnView.backgroundColor = UIColor.clear
            providerLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
            self.proContainerview.isHidden = false
            self.infoContainerview.isHidden = true
            self.collectionPopupView.isHidden = true
            self.Blurview.isHidden = true
         //   self.nameLbl.text = self.getname
        }

    @available(iOS 14.0, *)
    @IBAction func backBTNAction(_ sender: UIButton) {
      //  self.nameLbl.text?.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - @IBActions
extension GeneralContactProfileVC{
    
    @IBAction func profileBTNTapped(_ sender: UIButton) {
    }
    
    @IBAction func providerBTNTapped(_ sender: UIButton) {
        selectedBtn = 0
        infoBtnView.backgroundColor = UIColor.clear
        providerBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        providerLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.proContainerview.isHidden = false
        self.infoContainerview.isHidden = true
        self.collectionPopupView.isHidden = true
    }
    
    @IBAction func infoBTNTapped(_ sender: UIButton) {
        selectedBtn = 1
        providerBtnView.backgroundColor = UIColor.clear
        infoBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        providerLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.proContainerview.isHidden = true
        self.infoContainerview.isHidden = false
        self.collectionPopupView.isHidden = true
        
    }
    

    @IBAction func collectionBTNTapped(_ sender: UIButton) {
       // if collectionPopupView.isHidden{
            collectionPopupView.isHidden = false
            self.Blurview.isHidden = false
            self.collectionIMG.image = UIImage(named: "selectcollection")
        //}
//        else{
//            collectionPopupView.isHidden = true
//            self.collectionIMG.image = UIImage(named: "collection_icon")
//        }
        
        
    }
    
    @IBAction func collectionCloseTouchBTNTapped(_ sender : UIButton){
        if collectionPopupView.isHidden{
            collectionPopupView.isHidden = false
            Blurview.isHidden = false
            self.collectionIMG.image = UIImage(named: "selectcollection")

        }
        else{
            collectionPopupView.isHidden = true
            Blurview.isHidden = true
            self.collectionIMG.image = UIImage(named: "collection_icon")
        }
        
//        self.Blurview.isHidden = true
//        collectionPopupView.isHidden = true
    }
    
//MARK:- PopupView Actions
    @IBAction func inviteBTNTapped(_ sender: UIButton) {
        inviteapi()
    }
    
    @IBAction func shareBTNTapped(_ sender: UIButton) {
        let contact = createContact(fname: self.nameLbl.text ?? "", lname: "", mob: getmobile)
        
        do {
            try self.shareContacts(contacts: [contact])
        }
        catch {
            print("failed to share contact with some unknown reasons")
        }
    }    
    
    //MARK: - share contact card from  menu
    func createContact(fname:String,lname:String,mob:String) -> CNContact {
        // Creating a mutable object to add to the contact
        let contact = CNMutableContact()
        contact.imageData = NSData() as Data // The profile picture as a NSData object
        contact.givenName = fname
        contact.familyName = lname
        contact.phoneNumbers = [CNLabeledValue(
            label:CNLabelPhoneNumberiPhone,
            value:CNPhoneNumber(stringValue:mob))]
        
        return contact
    }
   
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "EditGeneralContactVC") as! EditGeneralContactVC
        vc.getcontactid = self.selecteduserid
        vc.getmobile = self.getuserdetails?.Data?.phoneNumber ?? ""
        vc.getname = self.getuserdetails?.Data?.name ?? ""
        vc.getcountry = self.getuserdetails?.Data?.countryCode ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: - SHARE INVTIE LINK WITH TYPE
extension GeneralContactProfileVC{
    func inviteapi(){
        
        let params: [String : Any] = ["id": selecteduserid]
        print("params",params)
        let url = kBaseUrl+"inviteContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            //             print("params:\(params)")
            //             print("response:\(response)")
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]
//                {
//                  print("responsedata :",responsedata)
//                    let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
//                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
//                        if responsedata["data"] as? String ?? "" == "Updated sucessfully"{
//                            self.invitetoShow()
//
//                        }else{
//                            let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
//                        }
//                        self.collectionPopupView.isHidden = true
//                        self.Blurview.isHidden = true
//                        }
//                                                  ))
//                            self.present(alert, animated: true)
//                }
                {
                    
                    print("responsedata :",responsedata)
                    if responsedata["data"] as? String == "Updated successfully"{
                        
                        self.showToast(message: "\(responsedata["data"] ?? "")", font: .systemFont(ofSize: 13.0))
                        
                        self.invitetoShow()
                        
                    }
                    else{
//                        self.collectionPopupView.isHidden = true
//                        self.Blurview.isHidden = true

                    }
                    self.collectionPopupView.isHidden = true
                    self.Blurview.isHidden = true
                }
//                {
//
//                    print("responsedata :",responsedata)
//                    if responsedata["data"] as? String == "Updated sucessfully"{
//
//
//
//                     //   print(number)
//                        self.invitetoShow()
//                    }
//                    else{
//                      //  hideActivityIndicator()
//
//                    }
//                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            }
        }
    
    
    func invitetoShow(){
        let num = self.getuserdetails?.Data?.phoneNumber ?? ""
        let country = self.getuserdetails?.Data?.countryCode ?? ""
        print(num)
        print(country)
        let number = country + num
        print(number)
        let sms = "sms:"+number+"&body="+invitelink
       // let sms = "sms:+919567155224&body="+invitelink
        
        let strURL = sms.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        UIApplication.shared.open(URL(string: strURL)!, options: [:], completionHandler: nil)
        
     }
  
}

//MARK: - API Call
extension GeneralContactProfileVC{
    func userdetailsapi(){
        
        let params = ["contactid": selecteduserid] as [String : Any]
        
        let url = kBaseUrl+"getGeneralContactProfileDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.getuserdetails = getGeneralProfileResponse(from:responsedata)
                      
                        infolist = self.getuserdetails
                        providerlistS = self.getuserdetails
                        NotificationCenter.default.post(name: Notification.Name("generaldetails"), object: nil)
                        
                        DispatchQueue.main.async {
                            self.nameLbl.text = self.getuserdetails?.Data?.name ?? ""
                                let ipimage = IPImage(text: self.nameLbl.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                            self.getmobile = self.getuserdetails?.Data?.phoneNumber ?? ""
                            
                            
                            if self.getuserdetails?.Data?.contactUserId != nil && self.getuserdetails?.Data?.isUser == true{
                                print("profile")
                                self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
                               }
//                            else{
//                                print("contact")
//                               self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactUserId?.profilePic ?? "")))
//
//                            }
                        }
                       
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }

    }
}
