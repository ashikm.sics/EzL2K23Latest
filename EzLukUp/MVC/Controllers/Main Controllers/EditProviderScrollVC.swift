//
//  EditProviderScrollVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 06/07/23.
//

import UIKit
import Alamofire
import SwiftUI



class EditProviderScrollVC: UIViewController {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    
    @IBOutlet weak var searchIMG: UIImageView!
    @IBOutlet weak var aboutTextview: UITextView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchLbl: UILabel!
    @IBOutlet weak var dismissBTN: UIButton!
    @IBOutlet weak var searchView: BaseView!
    @IBOutlet weak var catDropView: BaseView!
    @IBOutlet weak var categoryTV: UITableView!
    @IBOutlet weak var recomIMG: UIImageView!
    @IBOutlet weak var recomBTN: UIButton!
    
    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var recomLbl: UILabel!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var deleteBTN: UIButton!
 
    @IBOutlet weak var nameView: BaseView!
    @IBOutlet weak var numberview: BaseView!
    @IBOutlet weak var aboutview: BaseView!
    @IBOutlet weak var recomBTNView: BaseView!
  
    @IBOutlet weak var editpropopupview: BaseView!
    @IBOutlet weak var popupGotitBTN: UIButton!
   
    
    var editlist : EditProviderContactDetailsResponse?
    var EPSsearchArrRes = [getcatDataModel]()
    var EPSnewcatarray = [getcatDataModel]()
    var EPScategorylist : getCategoriesResponse?
    var EPSselectedcategoryID = [String]()
    var tagCatID = String()
    var checkedbtnstatus : Bool = false
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    var getmobile = ""
    var getcontactid = ""
    var needsreview : Bool = false
    var categoryobeystatus : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editpropopupstatuscheck()
        searchTF.delegate = self
        needsreviewdetails()
        
    }
    
    func needsreviewdetails(){
        if needsreview == true{
            self.nameView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.numberview.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.searchView.backgroundColor = #colorLiteral(red: 0.2196078431, green: 0.8235294118, blue: 0.662745098, alpha: 0.1991308962)
            self.searchView.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
            self.searchView.borderWidth = 1.0
            self.aboutview.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
            self.aboutview.borderWidth = 1.0
            self.recomIMG.image = UIImage(named: "needsreviewcheckunselect")
            self.Edit_getcategoryapi(contactphonNum: getmobile)
            self.getcontactdetailsapi()
            getRecomendedCountapi(catid: EPSselectedcategoryID)
        }
        else if categoryobeystatus == true{
            self.nameView.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
            self.numberview.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
            self.searchView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.searchView.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.searchView.borderWidth = 1.0
            self.aboutview.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.aboutview.borderWidth = 1.0
            self.recomIMG.image = UIImage(named: "unselectcheckbox")
            self.getcontactdetailsapi()
            self.getcategoryapi()
            getRecomendedCountapi(catid: EPSselectedcategoryID)
        }
        else{
            self.nameView.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
            self.numberview.backgroundColor = #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1)
            self.searchView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.searchView.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.searchView.borderWidth = 1.0
            self.aboutview.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.aboutview.borderWidth = 1.0
            self.recomIMG.image = UIImage(named: "unselectcheckbox")
            self.getcontactdetailsapi()
            self.Edit_getcategoryapi(contactphonNum: getmobile)
            getRecomendedCountapi(catid: EPSselectedcategoryID)
        }
    }

    func editpropopupstatuscheck(){
        if editproviderpopupstatuscheck == false{
          //  editproviderpopupstatus = true
            self.editpropopupview.isHidden = false
        }
        else{
           // editproviderpopupstatus = false
            self.editpropopupview.isHidden = true
        }
    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
  
    
    @IBAction func dismissBTNTapped(_ sender: UIButton) {
        self.searchLbl.isHidden = true
        self.searchLbl.text = ""
        self.searchTF.text = ""
        self.dismissBTN.isHidden = true
        self.searchTF.isHidden = false
        self.searchIMG.isHidden = false
    }
    
    
    @IBAction func saveBTNTapped(_ sender: UIButton) {
        editprovidercontactapi()
    }
    
    @IBAction func deleteBTNTapped(_ sender: UIButton) {
      //  deleteprovidercontactapi()
    }
    
    @IBAction func recommendedBTNTap(_ sender: UIButton) {
        if needsreview == true{
            if checkedbtnstatus == false {
                checkedbtnstatus = true
                self.recomIMG.image = UIImage(named: "needsreviewcheckselect")
                self.aboutview.isHidden = false
            }else{
                checkedbtnstatus = false
                self.recomIMG.image = UIImage(named: "needsreviewcheckunselect")
                self.aboutview.isHidden = true
            }
        }
        else if categoryobeystatus == true{
            if checkedbtnstatus == false {
                checkedbtnstatus = true
                self.recomIMG.image = UIImage(named: "selectcheckbox")
                self.aboutview.isHidden = false
            }else{
                checkedbtnstatus = false
                self.recomIMG.image = UIImage(named: "unselectcheckbox")
                self.aboutview.isHidden = true
            }
        }
        else{
            if checkedbtnstatus == false {
                checkedbtnstatus = true
                self.recomIMG.image = UIImage(named: "selectcheckbox")
                self.aboutview.isHidden = false
            }else{
                checkedbtnstatus = false
                self.recomIMG.image = UIImage(named: "unselectcheckbox")
                self.aboutview.isHidden = true
            }
        }
    }
    
    @IBAction func popupGotitBTNTapped(_ sender: UIButton) {
        self.editpropopupview.isHidden = true
        editproviderpopupstatuscheck = true
    }
    
    
}


//MARK: - TextfieldDelegates & TextviewDelegates
extension EditProviderScrollVC: UITextFieldDelegate,UITextViewDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == searchTF {
            guard let text = textField.text else { return true }
            let newText = (text as NSString).replacingCharacters(in: range, with: string)
            self.catDropView.isHidden = newText == ""

            self.EPSsearchArrRes = newText == "" ? self.EPSnewcatarray : self.EPSnewcatarray.filter({ $0.name?.range(of: newText, options: .caseInsensitive) != nil })
            self.categoryTV.reloadData()

        }
        return true
    }
}


//MARK: - TableviewDelegates
extension EditProviderScrollVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.EPSsearchArrRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categorytvcell.identifier) as! categorytvcell
        cell.categoryLbl.text = self.EPSsearchArrRes[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        EPSselectedcategoryID.removeAll()
        self.searchLbl.isHidden = false
        self.dismissBTN.isHidden = false
        self.searchTF.isHidden = true
        self.searchIMG.isHidden = true
        self.catDropView.isHidden = true
        self.searchLbl.text = self.EPSsearchArrRes[indexPath.row].name
        tagCatID = self.EPSsearchArrRes[indexPath.row].id ?? ""
        EPSselectedcategoryID.append(tagCatID)
        
    }
}

extension EditProviderScrollVC{
    //MARK: - getcontactsapi
    func getcontactdetailsapi() {
    
        let params: [String : Any] = ["contactid": getcontactid]
        let url = kBaseUrl+"getContactDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.editlist = EditProviderContactDetailsResponse(from:responsedata)
                       
                        self.nameTF.text = self.editlist?.Data?.name ?? ""
                        self.phoneTF.text = self.editlist?.Data?.phoneNumber ?? ""
                        self.aboutTextview.text = self.editlist?.Feedback?.feedback ?? ""
                        
                        if self.needsreview == true{
                            if self.aboutTextview.text == ""{
                                self.aboutview.isHidden = true
                                self.recomIMG.image = UIImage(named: "needsreviewcheckunselect")
                                self.checkedbtnstatus = false
                            }else{
                                self.aboutview.isHidden = false
                                self.recomIMG.image = UIImage(named: "needsreviewcheckselect")
                                self.checkedbtnstatus = true
                                //needsreviewcheckselect
                            }
                        }
                        else if self.categoryobeystatus == true{
                            if self.aboutTextview.text == ""{
                                self.aboutview.isHidden = true
                                self.recomIMG.image = UIImage(named: "unselectcheckbox")
                                self.checkedbtnstatus = false
                            }else{
                                self.aboutview.isHidden = false
                                self.recomIMG.image = UIImage(named: "selectcheckbox")
                                self.checkedbtnstatus = true
                            }
                        }
                        else{
                            if self.aboutTextview.text == ""{
                                self.aboutview.isHidden = true
                                self.recomIMG.image = UIImage(named: "unselectcheckbox")
                                self.checkedbtnstatus = false
                            }else{
                                self.aboutview.isHidden = false
                                self.recomIMG.image = UIImage(named: "selectcheckbox")
                                self.checkedbtnstatus = true
                            }
                        }
                        
                        if self.editlist?.Data?.categoryIds.first?.name != nil{
                           // self.dismissBTN.isHidden = false
                            if self.needsreview == true{
                                self.dismissBTN.isHidden = true
                                self.dismissBTN.isUserInteractionEnabled = false
                            }
                            else if self.categoryobeystatus == true{
                                print(self.editlist?.Data?.categoryIds.count ?? 0)

                                self.searchTF.isHidden = true
                                self.searchIMG.isHidden = true
                                self.catDropView.isHidden = true
                                self.searchLbl.isHidden = false
                                self.dismissBTN.isHidden = false
                                self.dismissBTN.isUserInteractionEnabled = true
                                    self.searchLbl.text = self.editlist?.Data?.categoryIds.first?.name
                                    self.tagCatID = self.editlist?.Data?.categoryIds.first?.id ?? ""
                                    self.EPSselectedcategoryID.append(self.tagCatID)
                            }
                            else{
                                print(self.editlist?.Data?.categoryIds.count ?? 0)
                                
                                self.searchTF.isHidden = true
                                self.searchIMG.isHidden = true
                                self.catDropView.isHidden = true
                                self.searchLbl.isHidden = false
                                self.dismissBTN.isHidden = true
                                self.dismissBTN.isUserInteractionEnabled = false
                                    self.searchLbl.text = self.editlist?.Data?.categoryIds.first?.name
                                    self.tagCatID = self.editlist?.Data?.categoryIds.first?.id ?? ""
                                    self.EPSselectedcategoryID.append(self.tagCatID)
                             
                            }
                           

                            
                        }
                        else{
                            self.searchLbl.text = ""
                            self.searchLbl.isHidden = true
                        }
                        
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
           }
     }
    
    //MARK: - category api
        func getcategoryapi() {
            
            let url = kBaseUrl+"getCategories"
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                self.EPScategorylist = getCategoriesResponse(from: responsedata)
                                self.EPSnewcatarray = self.EPScategorylist?.Data ?? []
                                DispatchQueue.main.async {
                                    self.EPSsearchArrRes = self.EPSnewcatarray
                                    self.categoryTV.reloadData()
                                }
                            }

                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
                 }
          }
    
    
    
    
    //MARK: - API Call
    func editprovidercontactapi(){
       
        let params: [String : Any] = [
                      "contactid": getcontactid,
                      "name": self.nameTF.text ?? "",
                      "recommend": checkedbtnstatus,
                      "feedback":  self.aboutTextview.text ?? "",
                      "categoryids": EPSselectedcategoryID,
                      "phonenumber":  self.phoneTF.text ?? "",
                      "countrycode": "+1"
                                     ]
        print(params)
        
        let url = kBaseUrl+"editProviderContact"
         AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
             switch (response.result) {
             case .success( let JSON):
                 if let responsedata =  JSON as? [String:Any]  {
                   print("responsedata :",responsedata)

                     let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                         if responsedata["message"] as? String == "Updated successfully"{

                             self.navigationController?.popViewController(animated: true)
                           
                         }else{
                             let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                         }
                         }
                         ))
                             self.present(alert, animated: true)
                     
                 }
                 case .failure(let error):
                     print("Request error: \(error.localizedDescription)")
             }
         }
    }
        
    
    //MARK: - Api for enabling and disabling check box (max recomendation count 2)
//    getRecommendCount
    
    func getRecomendedCountapi(catid:[String]) {
  //   showActivityIndicator()
        let params = [
            "userid": userid,
                "categoryids": catid] as? [String : Any]
         
        let url = kBaseUrl+"getRecommendCount"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            
            print(response)
            switch (response.result) {
                       case .success( let JSON):
                        if let responsedata =  JSON as? [String:Any]  {
                          //  hideActivityIndicator()
                            if responsedata["recomendCount"] as? Int ?? 0 >= 2{
                                print("checkbox is hidden")
                                self.recomIMG.isUserInteractionEnabled = false
                                self.recomBTN.layer.borderColor = #colorLiteral(red: 0.6677497029, green: 0.6677497029, blue: 0.6677497029, alpha: 1)
                                self.recomBTN.layer.borderWidth = 2
                                self.recomLbl.textColor = #colorLiteral(red: 0.6677497029, green: 0.6677497029, blue: 0.6677497029, alpha: 1)
                                self.recomIMG.isHidden = true
                                self.noteLbl.text = "Note - You have already recommended 2 Providers for this Category, hence cannot recommend more. If you want to recommend this provider then first remove recommendation for one of the other ones."
                            }
                            else{
                                print("checkbox show ")
                                
                                if needsreview == true{
                                    self.recomIMG.image = UIImage(named: "needsreviewcheckunselect")
                                }else{
                                    self.recomIMG.image = UIImage(named: "unselectcheckbox")
                                }
                                
                                self.recomBTN.isUserInteractionEnabled = true
                               // self.recomBTN.layer.borderColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
                                self.recomLbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                                self.recomIMG.isHidden = false
                                
                                self.noteLbl.text = "Note - Any number of Providers can be tagged to a Category, but maximum 2 can be Recommended for a Category."
                            }
                           }
               // needsreviewcheckselect
                        case .failure(let error):
                           print("Request error: \(error.localizedDescription)")
                    }
             }
      }
    
    
    //category api for tagged contacts
    func Edit_getcategoryapi(contactphonNum:String) {
    // showActivityIndicator()
        let params = [
            "contactFullPhoneNumber":contactphonNum] as? [String : Any]
 
        let url = kBaseUrl+"availableCategoriesForProvider"
           AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
               
            print(response)
            switch (response.result) {
                       case .success( let JSON):
                        if let responsedata =  JSON as? [String:Any]  {
                            self.EPScategorylist = getCategoriesResponse(from: responsedata)
                            self.EPSnewcatarray = self.EPScategorylist?.Data ?? []
                            DispatchQueue.main.async {
                                self.EPSsearchArrRes = self.EPSnewcatarray
                                
                                if self.needsreview == true{
                                    self.searchTF.isHidden = true
                                    self.searchIMG.isHidden = true
                                    self.catDropView.isHidden = true
                                    self.searchLbl.isHidden = false
                                    self.searchLbl.text = self.EPScategorylist?.Data.first?.name ?? ""
                                    self.tagCatID = self.EPScategorylist?.Data.first?.id ?? ""
                                    self.EPSselectedcategoryID.append(self.tagCatID)
                                    self.dismissBTN.isHidden = true
                                    self.dismissBTN.isUserInteractionEnabled = false
                                }
                                
                                self.categoryTV.reloadData()
                            }
                        }
               
                        case .failure(let error):
                           print("Request error: \(error.localizedDescription)")
                    }
             }
      }
    
    
     //MARK: - DELETE API Call
//        func deleteprovidercontactapi(){
//
//          //  let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
//            let params: [String : Any] = [
//                          "contactid": getcontactid ]
//
//            let url = kBaseUrl+"deleteContact"
//            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
//                 switch (response.result) {
//                    case .success( let JSON):
//                        if let responsedata =  JSON as? [String:Any]  {
//                          print("responsedata :",responsedata)
//                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
//                                if responsedata["message"] as? String == "Record removed successfully"{
//
//    //                                self.navigationController?.popViewController(animated: true)
//                                    if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
//                                            guard viewControllers.count < 3 else {
//                                                self.navigationController?.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
//                                                return
//                                            }
//                                        guard viewControllers.count < 2 else{
//                                            self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
//                                            return
//                                        }
//                                        }
//
//
//
//
//
//
//                                }else{
//                                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
//                                }
//                                }
//                                                          ))
//                                    self.present(alert, animated: true)
//
//                        }
//                        case .failure(let error):
//                            print("Request error: \(error.localizedDescription)")
//                    }
//             }
//        }
}




class categorytvcell: UITableViewCell{
    static let identifier = String(describing: categorytvcell.self)
    
    @IBOutlet weak var catbaseview: BaseView!
    @IBOutlet weak var categoryLbl: UILabel!
}
