//
//  FaqVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 09/03/23.
//

import UIKit
import Alamofire

class FaqVC: UIViewController {
    
    
    @IBOutlet weak var FaqTableview: UITableView!
    var faqData : FAQ_Base?
    var faqArray = [[String:Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FaqTableview.register(UINib(nibName: "FAQExpandableTableViewCell", bundle: nil), forCellReuseIdentifier: "FAQExpandableTableViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfaq(notification:)), name: Notification.Name("fromfaq"), object: nil)
        
    }
    
    @objc func onfaq(notification: Notification) {
        getFAQs()
    }
    
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
    }
    
    func getFAQs(){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        
        let params = ["":""] as [String : Any]
        
        let url = kBaseUrl+"getFaqs"
        AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            //print(response)
            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.faqData = FAQ_Base(from:responsedata)
                    if let dataa = responsedata["data"] as? [[String:Any]]{
                        //                        print("dataa = ", dataa)
                        
                        for i in dataa{
                            print("looped data =",i)
                            self.faqArray.append(i)
                        }
                    }
                    self.FaqTableview.reloadData()
                }
                
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            
            
        }
    }
    
}


extension FaqVC : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: FAQExpandableTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FAQExpandableTableViewCell", for: indexPath) as? FAQExpandableTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        var dict = faqArray[indexPath.row]
        cell.titleLabel.text = dict["question"] as? String
        cell.descriptionLabel.text = dict["answer"] as? String
        cell.topBorderView.isHidden = indexPath.row == 0
        cell.didTapOnExpandableCell = {
            tableView.beginUpdates()
            tableView.endUpdates()
//            tableView.scrollToRow(at: IndexPath(row: indexPath.row, section: indexPath.section), at: .bottom, animated: true)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
