//
//  ConsumerDashboardCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 06/04/23.
//

import UIKit
import Kingfisher
import Contacts
import ContactsUI

class ConsumerDashboardCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

//MARK: - First TableviewCell
class ConsumerListCell1: UITableViewCell{
    @IBOutlet weak var totalRewardsCountLbl: UILabel!
    @IBOutlet weak var contactCountLbl: UILabel!
    @IBOutlet weak var recommendCountLbl: UILabel!
    var ConsumerDashboardmodel : ConsumerDashboardResponse?
}

//MARK: - Second TableviewCell
class ConsumerListCell2: UITableViewCell{
    @IBOutlet weak var recommendCV: UICollectionView!
    @IBOutlet weak var recRecomEmptyview: UIView!
    
    @IBOutlet weak var recomCVheight: NSLayoutConstraint!
    var ConsumerDashboardmodel : ConsumerDashboardResponse?
}

//MARK: - Third TableviewCell
class ConsumerListCell3: UITableViewCell{
    @IBOutlet weak var feebbackReportCV: UICollectionView!
    @IBOutlet weak var recEmptyview: BaseView!
    var ConsumerDashboardmodel : ConsumerDashboardResponse?
  
    var type = ""
}

//MARK: - Fourth TableviewCell
class ConsumerListCell4: UITableViewCell{
    @IBOutlet weak var joinedContactsCV: UICollectionView!
    @IBOutlet weak var recJointEmptyview: UIView!
    
    @IBOutlet weak var viewAllBTN: UIButton!
    var ConsumerDashboardmodel : ConsumerDashboardResponse?
}


//MARK: - 1st Collectionview delegates
extension ConsumerListCell2:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if ConsumerDashboardmodel?.Data?.recentRecommendProviders.count ?? 0 <= 7{
        return ConsumerDashboardmodel?.Data?.recentRecommendProviders.count ?? 0
    }else{
        return 7
    }
    
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendationCVCell", for: indexPath) as! RecommendationCVCell
    cell.recomNameLbl.text = ConsumerDashboardmodel?.Data?.recentRecommendProviders[indexPath.item].name ?? ""
    cell.recomProfileIMG.kf.setImage(with: URL(string: kImageUrl + (self.ConsumerDashboardmodel?.Data?.recentRecommendProviders[indexPath.item].categoryIds.first?.categoryImage ?? "")),placeholder: UIImage(named: "profileicon"))
   
//
    if ConsumerDashboardmodel?.Data?.recentRecommendProviders[indexPath.item].categoryIds != nil{
        cell.categoryLbl.text = ConsumerDashboardmodel?.Data?.recentRecommendProviders[indexPath.item].categoryIds.first?.name ?? ""
    }else{
    cell.catView.isHidden = true
      //  cell.catView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        //cell.catviewheight.constant = 0
    }
    
    cell.mainViewBTN.tag = indexPath.row
    return cell
    }
    
//    func goChapter(sender: UIButton!) {
//        var point : CGPoint = sender.convertPoint(CGPointZero, toView:collectionView)
//        var indexPath = collectionView!.indexPathForItemAtPoint(point)
//        bookChapterVC.chapter = self.book?.bookChapters![indexPath.row]
//        navigationController?.pushViewController(bookChapterVC, animated: true)
//    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 2.3
             //   return CGSize(width:152, height: 212.0)
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
    
}


//MARK: - 2nd Collectionview delegates
extension ConsumerListCell3:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if ConsumerDashboardmodel?.Data?.recentFeedback.count ?? 0 <= 7{
        return ConsumerDashboardmodel?.Data?.recentFeedback.count ?? 0
    }else{
        return 7
    }
   // return ConsumerDashboardmodel?.Data?.recentFeedback.count ?? 0
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeebbackReportCVCell", for: indexPath) as! FeebbackReportCVCell
    cell.nameLbl.text = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].providerId?.fullName ?? ""
    cell.feedreportLbl.text = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].feedback ?? ""
   // cell.connectNameLbl.text = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].userId?.fullName ?? ""
    cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].userId?.profilePic ?? "")),placeholder: UIImage(named: "profileimgIfnodata"))
    
    if ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].categoryIds != nil{
        cell.catLbl.text = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].categoryIds?.name ?? ""
    }else{
        cell.catview.isHidden = true
    }
    
 
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    var name : String = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].userId?.fullName ?? ""
  
    if ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.firstLevelConnections!.contains(userid){
                cell.connectNameLbl.text = name + " .1st"
           // cell.connectNameLbl.text = "1st"
            }else if ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.secondLevelConnections!.contains(userid){
                cell.connectNameLbl.text = name + " .2nd"
              //  cell.connectNameLbl.text = "2nd"
            }
            else if ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId != nil && ConsumerDashboardmodel!.Data!.recentFeedback[indexPath.row].userId!.thirdLevelConnections!.contains(userid){
                cell.connectNameLbl.text = name + " .3rd"
               // cell.connectNameLbl.text = "3rd"
            }
        else{
            cell.connectNameLbl.text = name
        }

    
//MARK:- Feedback/Report border colouring
    type = ConsumerDashboardmodel?.Data?.recentFeedback[indexPath.item].type ?? ""
    if type == "feedback"{
        cell.mainview.borderColor = #colorLiteral(red: 0.2, green: 0.7960784314, blue: 0.5960784314, alpha: 1)
    }else{
        cell.mainview.borderColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
    }
    
    cell.mainviewBTN.tag = indexPath.row
    return cell
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 1.5
//                return CGSize(width: 200, height: 202)
        return CGSize(width: collectionView.frame.width / 1.7, height: collectionView.frame.height)
    }
}


//MARK: - 3rd Collectionview delegates
extension ConsumerListCell4:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 2
//    }
func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
   // if section == 0{
        if ConsumerDashboardmodel?.Data?.recentlyJoinedUsers.count ?? 0 <= 7{
            return ConsumerDashboardmodel?.Data?.recentlyJoinedUsers.count ?? 0
        }else{
            return 7
        }
   // }
//    else{
//        return 1
//    }

}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCVCell", for: indexPath) as! ContactCVCell
    cell.nameLbl.text = ConsumerDashboardmodel?.Data?.recentlyJoinedUsers[indexPath.item].contactUserId?.fullName ?? ""
//LocationSetup
    let city = ConsumerDashboardmodel?.Data?.recentlyJoinedUsers[indexPath.item].contactUserId?.city ?? ""
    let state = ConsumerDashboardmodel?.Data?.recentlyJoinedUsers[indexPath.item].contactUserId?.state ?? ""
    
    if city == "" && state == ""{
        cell.locationLbl.text =  ""
      }
      else{
          var samplecitystate = ""
          if city != "" && state != ""{
              samplecitystate = "\(city) , \(state)"
          }else{
              if city == ""{
                  samplecitystate = state
              }else{
                  samplecitystate = city
              }
          }
          cell.locationLbl.text =  samplecitystate
      }
    
       
    cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.ConsumerDashboardmodel?.Data?.recentlyJoinedUsers[indexPath.item].contactUserId?.profilePic ?? "")),placeholder: UIImage(named: "profileicon"))
    
        return cell
    
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w:CGFloat = (collectionView.frame.size.width - 8) / 1.5
               // return CGSize(width: 152, height: 212)
        return CGSize(width: collectionView.frame.width / 2.5, height: collectionView.frame.height)
    }
}


//MARK: - First CollectionviewCell
class RecommendationCVCell: UICollectionViewCell{
    @IBOutlet weak var recomProfileIMG: BaseImageView!
    @IBOutlet weak var recomNameLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var mainview: BaseView!
    @IBOutlet weak var mainViewBTN: UIButton!
    @IBOutlet weak var catView: BaseView!
    
}

//MARK: - Second CollectionviewCell
class FeebbackReportCVCell: UICollectionViewCell{
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var feedreportLbl: UILabel!
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var connectNameLbl: UILabel!
    @IBOutlet weak var mainview: BaseView!
    @IBOutlet weak var catview: BaseView!    
    @IBOutlet weak var mainviewBTN: UIButton!
}

//MARK: - Third Details CollectionviewCell
class ContactCVCell: UICollectionViewCell{
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var mainview: BaseView!
}

//MARK: - Third ContactSync CollectionviewCell
class AddContactCVCell: UICollectionViewCell{
    
}
