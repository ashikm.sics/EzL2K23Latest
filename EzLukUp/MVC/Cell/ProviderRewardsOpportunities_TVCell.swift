//
//  ProviderRewardsOpportunities_TVCell.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 12/04/23.
//

import UIKit

class ProviderRewardsOpportunities_TVCell: UITableViewCell {
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAmount:UILabel!
    @IBOutlet weak var bgView:UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        dropShadowForView(view: bgView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
