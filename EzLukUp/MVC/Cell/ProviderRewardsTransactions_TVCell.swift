//
//  ProviderRewardsTransactions_TVCell.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 11/04/23.
//

import UIKit

class ProviderRewardsTransactions_TVCell: UITableViewCell {
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var lblrewardType:UILabel!
    @IBOutlet weak var lblname:UILabel!
    @IBOutlet weak var lblrewardUpdatedDate:UILabel!
    @IBOutlet weak var lblRewardAmt:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        dropShadowForView(view: bgView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
