//
//  ProgressBar.swift
//  WayToNikkah
//
//  Created by Appzoc on 14/08/19.
//  Copyright © 2019 Mohamed Shafi. All rights reserved.
//

import UIKit


class ProgressBar : UIView {
    
    @IBInspectable var trackLayerColor : UIColor = .lightGray
    @IBInspectable var progressColor : UIColor = .cyan
    @IBInspectable var lineWidth : CGFloat = 5
    
    
    let shapeLayer = CAShapeLayer()
    override func awakeFromNib() {
        super.awakeFromNib()
       // makeLayer()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        makeLayer()
    }
    private func makeLayer() {
        
        
        //  let center = view.center
        let circularPath = UIBezierPath(arcCenter: .zero, radius: self.frame.size.width/2 - lineWidth/2, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        // create track layer
        let trackLayer = CAShapeLayer()
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = trackLayerColor.cgColor
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = .round
        trackLayer.lineWidth = lineWidth
        trackLayer.position = CGPoint(x: self.frame.size.width/2,
                                      y: self.frame.size.width/2)
        self.layer.addSublayer(trackLayer)
        
        // create progress LAyer
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = progressColor.cgColor
        shapeLayer.strokeEnd = 0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = .round
        shapeLayer.lineWidth = lineWidth
        shapeLayer.position = CGPoint(x: self.frame.size.width/2,
                                      y: self.frame.size.width/2)
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        self.layer.addSublayer(shapeLayer)
        
    }
    
    func setProgess(_ progress:CGFloat){
        DispatchQueue.main.async {
            self.shapeLayer.strokeEnd = progress
        }
    }
    
}


