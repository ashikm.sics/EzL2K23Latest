//
//  GeneralAlert.swift
//  DayToFresh
//
//  Created by Appzoc Technologies on 24/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class GeneralAlert {
    
    static let shared = GeneralAlert()
//    var tintColor : UIColor? = AppData.theme
    var alert = UIAlertController() {
        didSet {
//             self.alert.view.tintColor = tintColor
        }
    }
    
    private init(){}
    
    
    //MARK:- Alert Ok withOut callBack
    
    func generalAlert(controller:UIViewController,message:String,title:String? = "",actionTitle:String = "OK",alertStyle:UIAlertController.Style,_ handler:(()->())? = nil){
        self.alert = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        alert.addAction(UIAlertAction(title: actionTitle, style: .cancel, handler:{ _ in
            handler?()
        }))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Alert with Ok callBack
    
    func generalAlertWithCallBack(controller:UIViewController,message:String,title:String = "",actionTitle:String = "OK",alertStyle:UIAlertController.Style,_ handler:(()->())?){
         self.alert = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { _ in
            handler?()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Alert with OK Cancel CallBack
    func generalAlertWithBothActionsCallBack(controller:UIViewController,message:String,title:String = "",agreeTitle:String = "OK",denyTitle:String = "Cancel",alertStyle:UIAlertController.Style,_ agreeHandler:(()->())?,_ cancelHandler:(()->())?){
         self.alert = UIAlertController(title: title, message: message, preferredStyle: alertStyle)
        alert.addAction(UIAlertAction(title: agreeTitle, style: .default, handler: { _ in
            agreeHandler?()
        }))
        alert.addAction(UIAlertAction(title: denyTitle, style: .default, handler: { _ in
            cancelHandler?()
        }))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Alert with Auto dismiss
    
}
