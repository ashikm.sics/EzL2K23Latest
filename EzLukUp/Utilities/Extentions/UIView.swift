//
//  UIView.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

extension UIView {
    
    /* Usage Example
     * bgView.addBottomRoundedEdge(desiredCurve: 1.5)
     */
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
    
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
    
    func makeRoundedCorner(clipToBounds:Bool) {
        DispatchQueue.main.async {
            self.clipsToBounds = clipToBounds
            self.layer.cornerRadius = self.frame.width/2
        }
    }
    
    func makeHalfRoundCorner(clipToBounds:Bool) {
        DispatchQueue.main.async {
            self.clipsToBounds = clipToBounds
            self.layer.cornerRadius = self.frame.height/2
        }
    }
    
    func scaleViewTo(_ x:CGFloat,_ y:CGFloat,_ z:CGFloat) {
        self.layer.transform = CATransform3DMakeScale(x, y, z)
    }
    
    func removeScale() {
        UIView.animate(withDuration: 0.2) {
            self.layer.transform = CATransform3DMakeScale(1, 1, 1)
        }
    }
    
    func zoomInOutAniamtion(_ x:CGFloat = 1.1,_ y:CGFloat = 1.1,_ z:CGFloat = 1.1){
          scaleViewTo(x, y, z)
          removeScale()
      }
    
    func drawDottedLine(line dashPattern:[NSNumber],color:UIColor) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = dashPattern // 7 is the length of dash, 3 is length of the gap.
        let p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
        let p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
        func takeScreenshot() -> UIImage {
            
            // Begin context
            UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
            
            // Draw view in that context
            drawHierarchy(in: self.bounds, afterScreenUpdates: true)
            
            // And finally, get image
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            if (image != nil)
            {
                return image!
            }
            return UIImage()
        }
    
    func blur(shadowRadius: CGFloat, top: CGFloat, left: CGFloat, bottom: CGFloat, right: CGFloat, cornerRadius: CGFloat) {
           
           let maskLayerForBlur = CAGradientLayer()
           maskLayerForBlur.frame = self.bounds
           maskLayerForBlur.shadowRadius = shadowRadius
           maskLayerForBlur.shadowPath = CGPath(roundedRect: self.bounds.inset(by: UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)), cornerWidth: cornerRadius, cornerHeight: cornerRadius, transform: nil)

           maskLayerForBlur.shadowOpacity = 1
           maskLayerForBlur.shadowOffset = CGSize.zero
           maskLayerForBlur.shadowColor = UIColor.white.cgColor
           self.layer.mask = maskLayerForBlur
       // self.layoutSubviews()
       }
}
