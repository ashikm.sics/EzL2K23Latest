
import Foundation
import CoreData
import UIKit

class CoreDataManager {
  
  //1
  static let sharedManager = CoreDataManager()
  private init() {} // Prevent  from creating another instance.
  
  //2
  lazy var persistentContainer: NSPersistentContainer = {
    
    let container = NSPersistentContainer(name: "SampleAPI") //DB name
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
      
      if let error = error as NSError? {
        fatalError("Unresolved error \(error), \(error.userInfo)")
      }
    })
    return container
  }()
  
    func Deleteall(completion:@escaping() -> ()) { //Delete coredata values for general contact list
        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        let datafordelete = self.fetchAll() ?? []
        for data in datafordelete{
            context.delete(data)
        }
        completion()
    }
    
    func Deleteallprovider(completion:@escaping() -> ()) { //Delete coredata values for provider contact list
        let context = CoreDataManager.sharedManager.persistentContainer.viewContext
        let datafordelete = self.fetchAllprovider() ?? []
        for data in datafordelete{
            context.delete(data)
        }
        completion()
    }
    
  //3
  func saveContext () {
    let context = CoreDataManager.sharedManager.persistentContainer.viewContext
    if context.hasChanges {
      do {
        try context.save()
      } catch {
        
        let nserror = error as NSError
        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
      }
    }
  }
  
  /*Insert geral response to coredata*/
    func insertHomeresponse<T>(Response:T)->GeneralContactsData? {
        print("response\(Response)")

    let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext

    let entity = NSEntityDescription.entity(forEntityName: "GeneralContactsData",
                                            in: managedContext)!
    
    let homedata = NSManagedObject(entity: entity,
                                 insertInto: managedContext)
        homedata.setValue(Response, forKeyPath: "generalResponse")
    do {
      try managedContext.save()
      return homedata as? GeneralContactsData
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
      return nil
    }
  }
    
    /*Insert provider response to coredata*/
      func insertproviderresponse<T>(Response:T)->ProviderContactsData? {
          print("response\(Response)")

      let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
      let entity = NSEntityDescription.entity(forEntityName: "ProviderContactsData",
                                              in: managedContext)!

      let homedata = NSManagedObject(entity: entity,
                                   insertInto: managedContext)

          homedata.setValue(Response, forKeyPath: "providerResponse")
     
      do {
        try managedContext.save()
        return homedata as? ProviderContactsData
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        return nil
      }
    }
    
    func fetchAll() -> [GeneralContactsData]?{
      
      
      let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
      
      
      let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GeneralContactsData")
      
      
      do {
        let people = try managedContext.fetch(fetchRequest)
        return people as? [GeneralContactsData]
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
        return nil
      }
    }
    
    func fetchAllprovider() -> [ProviderContactsData]?{
      
      
     
      let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
      
      /*As the name suggests, NSFetchRequest is the class responsible for fetching from Core Data.
       
       Initializing a fetch request with init(entityName:), fetches all objects of a particular entity. This is what you do here to fetch all Person entities.
       */
      let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ProviderContactsData")
      
      /*You hand the fetch request over to the managed object context to do the heavy lifting. fetch(_:) returns an array of managed objects meeting the criteria specified by the fetch request.*/
      do {
        let people = try managedContext.fetch(fetchRequest)
        return people as? [ProviderContactsData]
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
        return nil
      }
    }
    
    
    func delete<T>(Response:T) -> [GeneralContactsData]? {
      /*get reference to appdelegate file*/
      
      
      /*get reference of managed object context*/
      let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
      
      /*init fetch request*/
      let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GeneralContactsData")
      
      /*pass your condition with NSPredicate. We only want to delete those records which match our condition*/
        fetchRequest.predicate = NSPredicate(format: "generalResponse == %@" ,Response as! CVarArg)
      do {
        
        /*managedContext.fetch(fetchRequest) will return array of person objects [personObjects]*/
        let item = try managedContext.fetch(fetchRequest)
        var arrRemovedPeople = [GeneralContactsData]()
        for i in item {
          
          /*call delete method(aManagedObjectInstance)*/
          /*here i is managed object instance*/
          managedContext.delete(i)
          
          /*finally save the contexts*/
          try managedContext.save()
          
          /*update your array also*/
          arrRemovedPeople.append(i as! GeneralContactsData)
        }
        return arrRemovedPeople
        
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
    return nil
      }
      
    }
    func updateresponse<T>(Response:T,userdata : [GeneralContactsData]) {
      
      
      let context = CoreDataManager.sharedManager.persistentContainer.viewContext
      
      do {
        
       
          for item in userdata{
          item.setValue(Response, forKey: "generalResponse")
          }
        
       
        do {
          try context.save()
          print("saved!")
        } catch let error as NSError  {
          print("Could not save \(error), \(error.userInfo)")
        } catch {
          
        }
        
      } catch {
        print("Error with request: \(error)")
      }
    }
}
