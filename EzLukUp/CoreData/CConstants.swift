//
//  Constants.swift
//  SampleAPI
//
//  Created by mac on 22/06/23.
//

import Foundation

struct Service {
    static let contentType = "application/json"
    static let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
}

struct Url {
  //  static let baseURL = "http://18.213.26.155/api"
    static let baseURL = "http://3.223.247.97/api/"
    static let contactsSingleApi = "getGeneralContactsSingleApi"
    static let providercontactsSingleApi = "getProviderContactsSingleApi"
}
