//
//  ProviderDataModel.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 13/08/23.
//

import Foundation


extension GeneralListData{
    func getcontactUserId() -> [GcontactUserID]{
        return DatabaseGcontactUserID().readAllgenerallistContactUuserID(contactID: self.id ?? "")
    }
    func getpossiblecontactUserId() -> [GcontactUserID]{
        return DatabaseGcontactUserID().readAllgenerallistContactUuserID(contactID: self.id ?? "")
    }
}
