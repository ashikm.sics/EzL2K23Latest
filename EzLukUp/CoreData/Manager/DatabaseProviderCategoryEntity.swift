//
//  DatabaseProviderCategoryEntity.swift
//  EzLukUp
//
//  Created by Srishti Innovative on 08/08/23.
//

import Foundation
import CoreData
class DatabaseProviderCategoryEntity: DataBaseManager{
    func insertprovidercategory(providercatdatas : ProviderSingleContactListResponse){
        //Provider category insert section
        for data in providercatdatas.data{
            let providercategory = self.insertNewRecords("ProviderCategoryEntity", context: self.privateObjectContext) as! ProviderCategoryEntity
            providercategory.reviewCount = Int16(providercatdatas.reviewCount ?? 0)
            providercategory.sectionOpened = true
            providercategory.id = data._id
            providercategory.name = data.name
            providercategory.categoryImage = data.categoryImage
            DatabaseProviderContactsEntity().insertProvidercontacts(Providercontacts: data.contacts, categoryid: data._id ?? "")
        }
        self.privateObjectContext.saveContext()
    }
    //Tagging section
    func insertfromGeneral(data : [String:Any] ,catid : String , catname : String){
        let providercategory = self.insertNewRecords("ProviderCategoryEntity", context: self.privateObjectContext) as! ProviderCategoryEntity
        providercategory.reviewCount =  0
        providercategory.sectionOpened = true
        providercategory.id = catid
        providercategory.name = catname
        providercategory.categoryImage = ""
        DatabaseProviderContactsEntity().insertfromGeneral(contact: data, catid: catid)
        self.privateObjectContext.saveContext()
    }
    //Read all provider from coredata
    func readAllproviderData() -> [ProviderCategoryEntity] {
      
        return self.readRecords(fromCoreData: "ProviderCategoryEntity", predicate: nil, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [ProviderCategoryEntity]
    }
}

class DatabaseProviderContactsEntity: DataBaseManager{
    // Provider contacts list insertion
    func insertProvidercontacts(Providercontacts:[ProviderModelC],categoryid : String){
        for contact in Providercontacts {
            let contacts = self.insertNewRecords("ProviderContactsEntity", context: self.privateObjectContext) as! ProviderContactsEntity
            contacts.providercatID = categoryid
            contacts.sectionOpened = true
            contacts.id = contact._id
            contacts.needCatagoryReview = contact.needCatagoryReview ?? false
            contacts.isUser = contact.isUser ?? false
            contacts.isRecommended = contact.isRecommended ?? false
            contacts.isReferred = contact.isReferred ?? false
            contacts.recommendedCount = Double(contact.recommendedCount)
            contacts.name = contact.name
            contacts.phoneNumber = contact.phoneNumber
            contacts.countryCode = contact.countryCode
            contacts.contactUserId = contact.contactUserId
            if let recomededlist = contact.recomended , let recomended  = recomededlist.first , let user = recomended.user , !user.isEmpty{
                contacts.profilepic = (user.map({$0.profilePic}) ) as NSObject
            }
            contacts.city = (contact.areaOfService.map({$0.city})) as NSObject
            contacts.state = (contact.areaOfService.map({$0.stateShortCode})) as NSObject
            contacts.country = (contact.areaOfService.map({$0.country})) as NSObject
//            DatabaseAreaofServiceEntity().insertProvidercontacts_areaofservice(Providercontacts_areaofservice: contact.areaOfService, contactID: contact._id ?? "")
        }
        self.privateObjectContext.saveContext()
    }
    // Tagging provider contacts
    func insertfromGeneral(contact: [String:Any], catid : String){
        let contacts = self.insertNewRecords("ProviderContactsEntity", context: self.privateObjectContext) as! ProviderContactsEntity
        contacts.providercatID = catid
        contacts.sectionOpened = true
        contacts.id = contact["_id"] as? String ?? ""
        contacts.needCatagoryReview = contact["needCatagoryReview"] as? Bool ?? false
        contacts.isUser = contact["isUser"] as? Bool ?? false
        contacts.isRecommended = contact["isRecommended"] as? Bool ?? false
        contacts.isReferred = contact["isReferred"] as? Bool ?? false
        contacts.recommendedCount = Double(contact["recommendedCount"] as? Int ?? 0)
        contacts.name = contact["name"] as? String ?? ""
        contacts.phoneNumber = contact["phoneNumber"] as? String ?? ""
        contacts.countryCode = contact["countryCode"] as? String ?? ""
        if let contactUserid = contact["contactUserId"] as? [String:Any] , let id = contactUserid["_id"] as? String {
            contacts.contactUserId = id
        }
        contacts.profilepic = ([""]) as NSObject
        if let areaofservice = contact["areaOfService"] as? [[String:Any]]{
            contacts.city = (areaofservice.map({$0["city"] as? String ?? ""})) as NSObject
            contacts.state = (areaofservice.map({$0["stateShortCode"] as? String ?? ""})) as NSObject
            contacts.country = (areaofservice.map({$0["country"] as? String ?? ""})) as NSObject
        }
        
        
        
        
        
        self.privateObjectContext.saveContext()
    }
    
    //  Reading full provider list
    func getfullcontactslist() -> [ProviderContactsEntity]{
        return self.readRecords(fromCoreData: "ProviderContactsEntity", predicate: nil, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [ProviderContactsEntity]
    }
    //  Reading particular provider datas from corresponding category
    func readAllproviderContacts(providercategory : String ) -> [ProviderContactsEntity] {
        let predicate = NSPredicate(format: "providercatID == %@", providercategory)
        return self.readRecords(fromCoreData: "ProviderContactsEntity", predicate: predicate, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [ProviderContactsEntity]
    }
}


//general list
enum ListType: Int {
    case taggedlist = 0,possiblelist,generallist
}


class DatabaseGeneralContactEntity: DataBaseManager{
    // general list insertion
    func insertgenerallist(generaldatas : [SingleContactListData],listtype : ListType){
        
        for data in generaldatas{
            let generalData = self.insertNewRecords("GeneralListData", context: self.privateObjectContext) as! GeneralListData
            generalData.id = data._id
            generalData.listType =  Double(listtype.rawValue)
            generalData.name = data.name
            generalData.isUser = data.isUser ?? false
            generalData.countryCode = data.countryCode
            generalData.phoneNumber = data.phoneNumber
            generalData.isReferred = data.isReferred ?? false
            generalData.isRecommended = data.isRecommended ?? false
            generalData.notInUs = data.notInUs ?? false
          
            if let userid = data.contactUserId{
                DatabaseGcontactUserID().insertcontactUserID(generalContactsUserID: userid, contactID: generalData.id ?? "")
            }
            
        }
        self.privateObjectContext.saveContext()
    }
    // Provider to general swipe function
    func providerTogeneral(Providercontacts:[ProviderModelC],categoryid : String){
        for data in Providercontacts{
            let generalData = self.insertNewRecords("GeneralListData", context: self.privateObjectContext) as! GeneralListData
            generalData.id = data._id
            generalData.listType =  2
            generalData.name = data.name
            generalData.isUser = data.isUser ?? false
            generalData.countryCode = data.countryCode
            generalData.phoneNumber = data.phoneNumber
            generalData.isReferred = data.isReferred ?? false
            generalData.isRecommended = data.isRecommended ?? false
            generalData.notInUs = data.notInUs ?? false
          
//            if let userid = data.contactUserId{
//                DatabaseGcontactUserID().insertcontactUserID(generalContactsUserID: userid, contactID: generalData.id ?? "")
//            }
            
        }
    }
    
    func readTaggedlist() -> [GeneralListData] {
        let predicate = NSPredicate(format: "listType == %d", 0)
              return self.readRecords(fromCoreData: "GeneralListData", predicate: predicate, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [GeneralListData]
    }
    func readpossiblelist() -> [GeneralListData] {
        let predicate = NSPredicate(format: "listType == %d", 1)
              return self.readRecords(fromCoreData: "GeneralListData", predicate: predicate, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [GeneralListData]
    }
    func readnormallist() -> [GeneralListData] {
        let predicate = NSPredicate(format: "listType == %d", 2)
              return self.readRecords(fromCoreData: "GeneralListData", predicate: predicate, sortDescriptor: ["name"], limit: 0, context: self.mainObjectContext) as! [GeneralListData]
    }
    func deletegeneralListData(_ Data: GeneralListData) {
            self.mainObjectContext.delete(Data)
            self.mainObjectContext.saveContext()
        }
}
class DatabaseGcontactUserID: DataBaseManager{
    func insertcontactUserID(generalContactsUserID:ContactUserIDModelCodable, contactID : String){
            let contactuserid = self.insertNewRecords("GcontactUserID", context: self.privateObjectContext) as! GcontactUserID
            contactuserid.id = generalContactsUserID._id
            contactuserid.generaluserID = contactID
            contactuserid.profilePic = generalContactsUserID.profilePic
        if let taggedcategories = generalContactsUserID.taggedCategories , let taggedcategory = taggedcategories.first , let innertaggedcat = taggedcategory.taggedCategory  {
            contactuserid.taggedname = innertaggedcat.name
            contactuserid.taggedId = innertaggedcat._id
            contactuserid.taggedcatimage = innertaggedcat.categoryImage
        }
        contactuserid.c_catidsCount = Double(generalContactsUserID.categoryIds?.count ?? 0)
        if let categoryIDs = generalContactsUserID.categoryIds ,let catids = categoryIDs.first {
            contactuserid.c_catid = catids._id
            contactuserid.c_catimg = catids.categoryImage
            contactuserid.c_catname = catids.name
        }
        contactuserid.recomended_Userid_count = Double(generalContactsUserID.recommendedUserId?.count ?? 0)
        
        if let recomededlist = generalContactsUserID.recommendedUserId {
            if generalContactsUserID.recommendedUserId?.count ?? 0 > 0{
                contactuserid.recomended_profilePic = recomededlist.map({$0.profilePic}) as NSObject
            }
        }

//        if let recomededlist = contact.recomended , let recomended  = recomededlist.first , let user = recomended.user , !user.isEmpty{
//            contacts.profilepic = (user.map({$0.profilePic}) ) as NSObject
//        }
       
        self.privateObjectContext.saveContext()
    }
    func readAllgenerallistContactUuserID(contactID : String ) -> [GcontactUserID] {
        let predicate = NSPredicate(format: "generaluserID == %@", contactID)
        return self.readRecords(fromCoreData: "GcontactUserID", predicate: predicate, sortDescriptor: nil, limit: 0, context: self.mainObjectContext) as! [GcontactUserID]
    }
    func readAllgeneralContactUuserID() -> [GcontactUserID] {
       
        return self.readRecords(fromCoreData: "GcontactUserID", predicate: nil, sortDescriptor: nil, limit: 0, context: self.mainObjectContext) as! [GcontactUserID]
    }
}



