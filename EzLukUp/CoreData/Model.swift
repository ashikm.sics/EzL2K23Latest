//
//  Model.swift
//  Model class for EZlukup
//
//  Created by mac on 26/06/23.
//

import Foundation
//MARK: - General list model
class SingleContactListResponse: Codable {
//    var apinormalList :[getcontactDataListModelcodable]? = []
    var status: Bool?
    var taggedContact : [SingleContactListData]?
    var taggedContactCount : Int?
    var possibleProviders : [SingleContactListData]?
    var possibleProvidersCount : Int?
    var normalList : [SingleContactListData]? = []
    var normalListCount : Int?
}

class getcontactDataListModelcodable:Equatable{
    static func == (lhs: getcontactDataListModelcodable, rhs: getcontactDataListModelcodable) -> Bool {
        return true
    }

    
    var sectionOpened : Bool? = true
    var Headername : String?
    var contactsList : [GeneralListData]? = []
    init(headername: String, contacts: [GeneralListData] ) {
        self.Headername = headername
        self.contactsList = contacts
//            .filter({!($0.isprovider ?? true)})
        self.sectionOpened = true
    }
    
    
    func switchCellexpansionStatus() {
        self.sectionOpened =  !(self.sectionOpened!)
        let managedContext = CoreDataManager.sharedManager.persistentContainer.viewContext
        do {
            try managedContext.save()
        }
        catch{
            print(error)
        }
    }
    
}

struct SingleContactListData: Codable {
    var sectionOpened : Bool? = true
    var isprovider : Bool? = false
    var _id: String = ""
    var userId: String = ""
    var name:String? = ""
    var  contactUserId :ContactUserIDModelCodable?
    var city: String?
    var stateShortCode :String?
    var  notInUs: Bool?
    var  isRecommended :Bool?
    var  isReferred :Bool?
    var  isUser :Bool?
    var countryCode : String?
    var  phoneNumber :String?
    var contactFullPhoneNumber : String?
    var  twilioCallerName :String?
    var  twilioCarrierType :String?
    var  type :String?
    var  contactImage:String?
    var recommendedCount :Int?
    var taggedCategoryIds : [taggedcatlistcodable]? = []
    //
}
struct ContactUserIDModelCodable: Codable{
    var _id : String?
    var fullName : String?
    var isActive : Bool?
    var isDeleted : Bool?
    var isSignup : Bool?
    var profilePic : String?
    var refCountryCode : String?
    var refPhoneNumber : String?
    var recommendedUserId :[recommendedUserIdListcodable]? = []
    var taggedCategories : [tagcatlistsectiononecodable]? = []
    var categoryIds : [taggedcatlistC]? = []
    var type : String?
    var city : String?
    var state :  String?
}
struct taggedcatlistC: Codable{
    var _id :String?
    var categoryImage :String?
    var name :String?
}
struct phoneNumberLocationModelCodable: Codable{
    var id : String?
    var area_code : String?
    var city : String?
    var city_short_code : String?
}
struct taggedcatlistcodable: Codable{
    var _id :String?
    var categoryImage :String?
    var name :String?
}
struct recommendedUserIdListcodable: Codable{
    var profilePic : String?
}
struct tagcatlistsectiononecodable: Codable{
    var count : Int?
    var taggedCategory : taggedcatlistcodable?
}
//MARK: - provider list model
class ProviderSingleContactListResponse: Codable {
    var status: Bool?
    var data : [ProviderCategoryListModel] = []
    var reviewCount : Int?
    
}

struct ProviderCategoryListModel: Codable{
    var _id :String? = ""
    var name :String? = ""
    var categoryImage :String? = ""
    var contacts : [ProviderModelC] = []
    
}
class ProviderCategoryListModelC{
    var sectionOpened :Bool = true
    var data : ProviderCategoryEntity?
    lazy var contacts: [ProviderContactsEntity] = {
        return self.getprovidercontact()
    }()
    
    init(data: ProviderCategoryEntity? = nil) {
        self.sectionOpened = true
        self.data = data
    }
     func switchCellexpansionStatus() {
         self.sectionOpened =  !(self.sectionOpened)
        
    }
    func getprovidercontact() -> [ProviderContactsEntity]{
        return DatabaseProviderContactsEntity().readAllproviderContacts(providercategory: data?.id ?? "")
       
    }
}

struct ProviderModelC: Codable{
    var _id :String? = ""
    var userId :String? = ""
    var name :String? = ""
    var countryCode :String? = ""
    var phoneNumber :String? = ""
    var isUser :Bool?
    var contactUserId :String? = ""
    var contactFullPhoneNumber :String? = ""
    var isReferred :Bool?
    var type :String? = ""
    var isRecommended :Bool?
    var categoryIds :[String]? = []
    var notInUs :Bool?
    var needCatagoryReview :Bool? = false
    var recommendedCount : Int = 0
    var areaOfService : [areaofserviceModelC] = []
    var recomended : [recomendedModelC]? = []
}
struct recomendedModelC:Codable{
    var _id : String = ""
    var fullName : String = ""
    var refCountryCode : String = ""
    var refPhoneNumber : String = ""
    var profilePic : String = ""
    var user : [recUserModelC]? = []
}
struct recUserModelC:Codable{
    var _id : String = ""
    var fullName : String = ""
    var refCountryCode : String = ""
    var refPhoneNumber : String = ""
    var profilePic : String = ""
}
struct areaofserviceModelC:Codable{
    var country : String = ""
    var city : String = ""
    var stateShortCode : String = ""
    var radius : String = ""
    var _id : String = ""
}
















