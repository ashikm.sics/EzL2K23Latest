
import Foundation
import CoreData

 class DataBaseManager {
     
     
    
    func readRecords(fromCoreData tableName: String, predicate: NSPredicate?, sortDescriptor: [String]?, ascending: Bool = true, limit: Int, offset: Int = 0, context: NSManagedObjectContext) -> [Any] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: tableName, in: context)
        fetchRequest.entity = entity
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.fetchOffset = offset
        if predicate != nil {
            fetchRequest.predicate = predicate
        }
        if let sortkeys = sortDescriptor {
            var sortedItems: [NSSortDescriptor] = []
            for key in sortkeys {
                let sortDescriptor = NSSortDescriptor(key: key, ascending: ascending)
                sortedItems.append(sortDescriptor)
            }
            fetchRequest.sortDescriptors = sortedItems
        }
        if limit != 0 {
            fetchRequest.fetchLimit = limit
        }
        let records: [Any]? = try? context.fetch(fetchRequest)
        return records!
    }
    
     func deleteAllRecords(fromCoreData tableName: String, context: NSManagedObjectContext) throws {
         let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: tableName)
         let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

         do {
             try context.execute(deleteRequest)
             try context.save()
         } catch let error as NSError {
             throw error
         }
     }
    
    func insertNewRecords(_ tableName: String, context: NSManagedObjectContext) -> Any? {
        let table: Any? = NSEntityDescription.insertNewObject(forEntityName: tableName, into:  context)
        return table
    }
    

    
    func countOfUsers(_ table: String, context: NSManagedObjectContext? = nil) -> Int {
        guard let contextObject = context else {
            return self.fetchUserCount(table, context: self.mainObjectContext)
        }
        return self.fetchUserCount(table, context: contextObject)
    }
    
    func fetchUserCount(_ table: String, context: NSManagedObjectContext) -> Int {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: table, in: context)
        fetchRequest.entity = entity
        do {
            let count = try context.count(for: fetchRequest)
            return count
        } catch {
            return 0
        }
    }
    
    func convertManagedObjectsToMainContext(_ managedObjects: Array<NSManagedObject>) -> [NSManagedObject] {
        if managedObjects.count == 0 {
            return managedObjects
        }
        var mainObjects = [NSManagedObject]()
        for item in managedObjects {
            mainObjects.append(self.mainObjectContext.object(with: item.objectID))
        }
        return mainObjects
    }
    
    lazy var mainObjectContext: MainManagedObjectContext = {
        return CoreData.sharedCoreData.mainManagedObjectContext
    }()
    
    lazy var privateObjectContext: PrivateManagedObjectContext = {
        let context = PrivateManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.mainObjectContext
        context.mergePolicy = NSMergePolicy.mergeByPropertyObjectTrump
        return context
    }()
}

