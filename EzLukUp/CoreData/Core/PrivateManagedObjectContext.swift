//
//  PrivateManagedObjectContext.swift
//  EZCapture
//
//  Created by Richin.C on 24/06/20.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
import CoreData

class PrivateManagedObjectContext: NSManagedObjectContext {
    
    func saveContext() {
        if self.hasChanges {
            do { try self.save() }
            catch { abort() }
        }
    }
    
    override func save() throws {
        try super.save()
        if let mainObjectContext = self.parent as? MainManagedObjectContext {
            mainObjectContext.saveContext()
        }
    }
}
