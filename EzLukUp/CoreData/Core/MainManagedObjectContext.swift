//
//  MainManagedObjectContext.swift
//  EZCapture
//
//  Created by Richin.C on 24/06/20.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit
import CoreData

class MainManagedObjectContext: NSManagedObjectContext {
    
    func saveContext() {
        self.perform {
            if self.hasChanges {
                do { try self.save() }
                catch { abort() }
            }
        }
    }
    
    override func save() throws {
        try super.save()
        self.saveMasterObjectContext()
    }
    
    func saveMasterObjectContext() {
        if let masterContext = self.parent, UIApplication.shared.applicationState != .background {
            masterContext.perform {
                if masterContext.hasChanges {
                    do { try masterContext.save() }
                    catch { abort() }
                }
            }
        }
    }
}
