//
//  WSManager.swift
//  SampleAPI
//
//  Created by mac on 22/06/23.
//

import Foundation
import Alamofire

class WSManager {
    func makeRequest<T: Codable>(url: String,decodeItem: T.Type, method: HTTPMethod, params: [String: Any]?, callback: @escaping (T?, Error?) -> ()) {
        var header: HTTPHeaders = ["Content-Type": Service.contentType, "Accept-Type": Service.contentType]
        header["x-access-token"] = Service.token
        AF.request(kBaseUrl + url, method: method, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: header).responseDecodable(of: decodeItem) { response in
            switch response.result {
            case .success(let data):
                callback(data, nil)
            case .failure(let error):
                callback(nil, error)
            }
        }
    }
}
